<?php

/**
 * StringArray.php
 */
namespace PiecesPHP\Core\DataStructures;


/**
 * StringArray - Representación de un array de strings
 * 
 * Funciona como módulo independiente
 * @category 	DataStructures
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class StringArray extends ArrayOf
{

	/**
	 * __construct
	 *
	 * @param mixed $input Un string o array que solo contenga strings
	 * 
	 * @throws NotAllowedTypeException
	 */
	public function __construct($input = [])
	{
		parent::__construct($input, self::TYPE_STRING, null);
	}
}
