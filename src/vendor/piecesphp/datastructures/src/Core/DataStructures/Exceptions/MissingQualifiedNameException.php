<?php

/**
 * MissingQualifiedNameException.php
 */
namespace PiecesPHP\Core\DataStructures\Exceptions;

/**
 * MissingQualifiedNameException - ....
 *
 * @category     Exceptions
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info No funciona como módulo independiente
 */
class MissingQualifiedNameException extends \Exception
{
    /**
     * __construct
     *
     * @param \Throwable $previous
     */
    public function __construct(int $code = 0, \Throwable $previous = null)
    {
        parent::__construct('missing_qualified_name', $code, $previous);
    }
}
