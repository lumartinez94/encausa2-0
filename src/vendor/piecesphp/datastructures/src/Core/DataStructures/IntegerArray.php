<?php

/**
 * IntegerArray.php
 */
namespace PiecesPHP\Core\DataStructures;


/**
 * IntegerArray - Representación de un array de integers
 * 
 * Funciona como módulo independiente
 * @category 	DataStructures
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class IntegerArray extends ArrayOf
{

	/**
	 * __construct
	 *
	 * @param mixed $input Un integer o array que solo contenga integers
	 * 
	 * @throws NotAllowedTypeException
	 */
	public function __construct($input = [])
	{
		parent::__construct($input, self::TYPE_INTEGER, null);
	}
}
