<?php

/**
 * ArrayOf.php
 */
namespace PiecesPHP\Core\DataStructures;

use PiecesPHP\Core\DataStructures\Exceptions\NotAllowedTypeException;
use PiecesPHP\Core\DataStructures\Exceptions\MissingQualifiedNameException;


/**
 * ArrayOf - Representación de un array tipado
 * 
 * Funciona como módulo independiente
 * @category 	DataStructures
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class ArrayOf extends \ArrayObject
{
	/**
	 * TYPE_INTEGER
	 * 
	 * Constante para establecer el tipo como integer
	 *
	 * @var string
	 */
	const TYPE_INTEGER = 'integer';

	/**
	 * TYPE_DECIMAL
	 * 
	 * Constante para establecer el tipo como double
	 *
	 * @var string
	 */
	const TYPE_DECIMAL = 'double';

	/**
	 * TYPE_STRING
	 * 
	 * Constante para establecer el tipo como string
	 *
	 * @var string
	 */
	const TYPE_STRING = 'string';

	/**
	 * TYPE_OBJECT
	 * 
	 * Constante para establecer el tipo como object
	 * 
	 * Este valor establece que los elementos de entrada sean objetos y requiere
	 * que se defina en el constructuctor el nombre cualificado de la clase que adminitirá
	 *
	 * @var string
	 */
	const TYPE_OBJECT = 'object';

	/**
	 * TYPE_SELF
	 * 
	 * Constante para establecer el tipo como self
	 * 
	 * Este valor establece que los elementos de entrada sean instancias de si mismo
	 *
	 * @var string
	 */
	const TYPE_SELF = 'self';

	/**
	 * $type
	 *
	 * @var string
	 */
	protected $type = 'string';

	/**
	 * $qualifiedName
	 *
	 * @var string
	 */
	protected $qualifiedName = null;

	/**
	 * __construct
	 *
	 * @param mixed $input Elemento(s) de entrada, puede ser un elemento del tipo admitido o un array
	 * que contenga elementos únicamente del tipo admitido.
	 * @param string $type Tipo de elemento aceptado. Están definidos en la constanstes de clase.
	 * Si el TYPE_OBJECT entonces debe ser suministrado el nombre cualificado del objeto.
	 * @param mixed $qualifiedName Nombre cualificado del objeto. Puede ser obtenido
	 * con la nomenclatura NombreDelObjeto::class
	 * @throws NotAllowedTypeException|MissingQualifiedNameException
	 */
	public function __construct($input = [], string $type, string $qualifiedName = null)
	{

		$this->validateType($type);
		
		$this->type = $type;

		if ($this->type === self::TYPE_OBJECT) {
			if ($qualifiedName !== null) {
				$this->qualifiedName = $qualifiedName;
			} else {
				throw new MissingQualifiedNameException();
			}
		}

		if (!is_array($input)) {
			$input = [$input];
		}

		foreach ($input as $item) {
			$this->validateInput($item);
		}

		parent::__construct($input);
	}

	/**
	 * append
	 *
	 * @param mixed $input Elemento del tipo admitido
	 * @throws NotAllowedTypeException
	 */
	public function append($input)
	{
		$this->validateInput($input);

		parent::append($input);
	}

	/**
	 * exchangeArray
	 * 
	 * Intercambia el array por otro
	 *
	 * @param mixed $input Elemento(s) de entrada, puede ser un elemento del tipo admitido o un array
	 * que contenga elementos únicamente del tipo admitido.
	 * @return array El array anterior
	 * @throws NotAllowedTypeException
	 */
	public function exchangeArray($input)
	{
		if (is_array($input)) {
			foreach ($input as $item) {
				$this->validateInput($item);
			}
		} else {
			$this->validateInput($input);
		}
		parent::exchangeArray($input);
	}

	/**
	 * offsetSet
	 *
	 * @param mixed $index Índice a ser establecido
	 * @param mixed $value Elemento de entrada
	 * @return void
	 * @throws NotAllowedTypeException
	 */
	public function offsetSet($index, $value)
	{
		$this->validateInput($value);

		parent::offsetSet($index, $value);
	}

	/**
	 * validateType
	 *
	 * @param string $type
	 * @return void 
	 * @throws NotAllowedTypeException
	 */
	protected function validateType(string $type)
	{
		$valid;

		switch ($type) {
			case self::TYPE_INTEGER:
			case self::TYPE_DECIMAL:
			case self::TYPE_STRING:
			case self::TYPE_OBJECT:
			case self::TYPE_SELF:
				$valid = true;
				break;
			default:
				$valid = false;
		}

		if(!$valid){			
			throw new NotAllowedTypeException();
		}
	}

	/**
	 * validateInput
	 *
	 * @param mixed $input
	 * @return void	 * 
	 * @throws NotAllowedTypeException
	 */
	protected function validateInput($input)
	{
		$valid;

		switch ($this->type) {
			case self::TYPE_INTEGER:
				$valid = is_integer($input);
				break;
			case self::TYPE_DECIMAL:
				$valid = is_float($input);
				break;
			case self::TYPE_STRING:
				$valid = is_string($input);
				break;
			case self::TYPE_OBJECT:
				$valid = $input instanceof $this->qualifiedName;
				break;
			case self::TYPE_SELF:
				$class_name = get_class($this);
				$valid = $input instanceof $class_name;
				break;
			default:
				$valid = false;
				break;
		}

		if (!$valid) {
			throw new NotAllowedTypeException();
		}
	}

	/**
	 * getType
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * getQualifiedName
	 *
	 * @return string
	 */
	public function getQualifiedName()
	{
		return $this->qualifiedName;
	}

	/**
	 * __toString
	 *
	 * @return string
	 */
	public function __toString()
	{
		$elements = $this->getArrayCopy();

		if ($this->type === self::TYPE_OBJECT) {
			$elements = array_map(function ($e) {
				return serialize($e);
			}, $elements);
		}

		return json_encode($elements);
	}
}
