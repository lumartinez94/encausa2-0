<?php

require_once __DIR__ . '/requires.php';

use PiecesPHP\Core\HTML\HtmlElement;

$element = new HtmlElement('a', 'Texto');
$element->setAttribute('href', 456);
$element->setAttribute('class', 'a b c d e');

$classAttribute = $element->getAttributeInstance('class');

if (!is_null($classAttribute)) {
    $classAttribute->appendValue('f');
    $classAttribute->appendValue('g');
}

$element->getAttributes(false)->offsetGet('class')->appendValue('h');

$code_text = <<<EOT
use PiecesPHP\Core\HTML\HtmlElement;

\$element = new HtmlElement('a', 'Texto');
\$element->setAttribute('href', 456);
\$element->setAttribute('class', 'a b c d e');

\$classAttribute = \$element->getAttributeInstance('class');

if (!is_null(\$classAttribute)) {
    \$classAttribute->appendValue('f');
    \$classAttribute->appendValue('g');
}

\$element->getAttributes(false)->offsetGet('class')->appendValue('h');

//Output
EOT;

$dump = var_export([
    $element->getAttributes(false),
    $element->getAttribute('href'),
    $element->getAttribute('href', false),
    $element->removeAttribute('href'),
    $element->getAttribute('href'),
    $element->getAttribute('href', false),
    $element->getAttribute('class'),
    (string)$element->getAttributes(false)->offsetGet('class'),
    $element->getAttribute('class', false),
    $element->getText(),
    $element->innerHTML(),
], true);

highlight_string("<?php\r\n$code_text\r\n\r\n$dump\r\n?>");
