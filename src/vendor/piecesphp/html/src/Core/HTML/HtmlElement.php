<?php

/**
 * HtmlElement.php
 */
namespace PiecesPHP\Core\HTML;

use PiecesPHP\Core\HTML\Attribute;
use PiecesPHP\Core\HTML\Collections\AttributeArray;
use PiecesPHP\Core\HTML\Collections\ElementArray;
use PiecesPHP\Core\HTML\Exceptions\MalformedChildException;
use PiecesPHP\Core\HTML\Interfaces\Element;

/**
 * HtmlElement - Elemento html
 *
 * Funciona como módulo independiente
 * @category     HTML
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
class HtmlElement implements Element
{
    /**
     * $uniqueID
     *
     * @var string
     */
    protected $uniqueID;
    /**
     * $tag
     *
     * @var string
     */
    protected $tag;

    /**
     * $isDoubleTag
     *
     * @var boolean
     */
    protected $isDoubleTag;

    /**
     * $attributes
     *
     * @var AttributeArray
     */
    protected $attributes = null;

    /**
     * $childs
     *
     * @var ElementArray
     */
    protected $childs = null;

    /**
     * $innerHTML
     *
     * @var string
     */
    protected $innerHTML = '';

    /**
     * $text
     *
     * @var string
     */
    protected $text = '';

    /**
     * __construct
     *
     * @param string $tag
     * @param string $text
     * @param ElementArray|array<Element>|Element $childs
     * @param AttributeArray|Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value'] | ['attributo'=>['value','value2'...]] [Attribute,...]
     * @param bool $doubleTag
     * @throws MalformedChildException
     */
    public function __construct(string $tag, string $text = '', $childs = null, $attributes = null, bool $doubleTag = true)
    {
        $this->tag = $tag;
        $this->text = $text;
        $this->setChilds($childs);
        $this->setAttributes($attributes);
        $this->isDoubleTag = $doubleTag;
        $this->uniqueID = uniqid(time());
    }

    /**
     * appendChild
     *
     * @param Element $child
     * @return static
     */
    public function appendChild(Element $child)
    {
        $this->childs->append($child);
        return $this;
    }

    /**
     * setAttribute
     *
     * @param string $name
     * @param mixed $value
     * @param string $separator
     * @return static
     */
    public function setAttribute(string $name, $value = null, string $separator = null)
    {
        if (is_null($separator)) {
            $this->attributes->append(new Attribute($name, $value, ' '));
        } else {
            $this->attributes->append(new Attribute($name, $value, $separator));
        }

        return $this;
    }

    /**
     * setAttributes
     *
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value']
     * o
     * ['attributo'=>['value','value2'...]]
     * @return static
     */
    protected function setAttributes($attributes = null)
    {
        if (is_array($attributes)) {

            $this->attributes = new AttributeArray($this->createAttributes($attributes));
        } else if ($attributes instanceof Attribute) {

            $this->attributes->append($attributes);
        }

        $this->attributes = new AttributeArray();

        if ($attributes instanceof AttributeArray) {

            $this->attributes = $attributes;
        } else if ($attributes instanceof Attribute) {

            $this->attributes->append($attributes);
        } else if (is_array($attributes)) {

            $this->attributes = new AttributeArray($this->createAttributes((array)$attributes));
        }
        return $this;
    }

    /**
     * setChilds
     *
     * @param ElementArray|array<Element>|Element $childs
     * @return static
     * @throws MalformedChildException
     */
    protected function setChilds($childs = null)
    {
        $this->childs = new ElementArray();

        if ($childs instanceof ElementArray) {

            $this->childs = $childs;
        } elseif ($childs instanceof Element) {

            $this->childs->append($childs);
        } elseif (is_array($childs)) {

            foreach ($childs as $child) {

                if ($child instanceof Element) {

                    $this->childs->append($child);
                } else {

                    throw new MalformedChildException();
                }
            }
        } elseif (!is_null($childs)) {
            throw new MalformedChildException();
        }
        return $this;
    }

    /**
     * getAttributes
     *
     * @param bool $outputAsString Si es true devuelve una cadena con los atributos, si es false;
     * un array con cada atributo como índice y sus valores como un array.
     * @return string|AttributeArray
     */
    public function getAttributes(bool $outputAsString = true)
    {
        $attrs = [];

        if ($outputAsString) {

            foreach ($this->attributes as $attr) {

                $attrs[] = $attr;
            }

            $attrs = trim(implode(' ', $attrs));

            return $attrs;
        } else {

            return $this->attributes;
        }
    }

    /**
     * getAttribute
     *
     * @param string $name El nombre del atributo
     * @param bool $outputAsString Si es true devuelve el valor como cadena, si es false; como un array.
     * @return string|array  Si no existe, devuelve null
     */
    public function getAttribute(string $name, bool $outputAsString = true)
    {
        /**
         * @var bool $attrExists
         */
        $attrExists = $this->attributes->offsetExists($name);

        if ($attrExists) {

            /**
             * @var Attribute $attr
             */
            $attr = $this->attributes->offsetGet($name);

            if ($outputAsString) {
                return $attr->getValue();
            } else {
                return $attr->getValue(false);
            }
        } else {
            return null;
        }
    }



    /**
     * getAttributeInstance
     *
     * @param string $name El nombre del atributo
     * @return Attribute|null  Si no existe devuelve null
     */
    public function getAttributeInstance(string $name)
    {
        /**
         * @var bool $attrExists
         */
        $attrExists = $this->attributes->offsetExists($name);

        if ($attrExists) {

            /**
             * @var Attribute $attr
             */
            $attr = $this->attributes->offsetGet($name);

            return $attr;
        } else {
            return null;
        }
    }

    /**
     * removeAttribute
     *
     * @param string $name
     * @return bool true si fue eliminado, false si no existe
     */
    public function removeAttribute(string $name)
    {
        if ($this->attributes->offsetExists($name)) {
            $this->attributes->offsetUnset($name);
            return true;
        } else {
            return false;
        }
    }

    /**
     * getText
     *
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * getUniqueID
     *
     * @return string
     */
    public function getUniqueID(): string
    {
        return $this->uniqueID;
    }

    /**
     * innerHTML
     *
     * @param mixed $html
     * @return void|string
     */
    public function innerHTML(Element $html = null)
    {
        if ($html === null) {

            $contents = [];

            foreach ($this->childs as $child) {
                $contents[] = $child->render(false);
            }

            $innerHTML = '';

            $innerHTML .= $this->getText();
            $innerHTML .= implode("", $contents);

            return $innerHTML;
        } else {

            $this->childs = [];
            $this->childs[$html->getUniqueID()] = $html;
        }
    }

    /**
     * render
     *
     * @param bool $echo
     * @return string
     */
    public function render($echo = true): string
    {
        $attributes = $this->getAttributes();

        $open_tag = '';
        $close_tag = "</$this->tag>";

        if (strlen($attributes) > 0) {
            $open_tag = "<$this->tag $attributes>";
        } else {
            $open_tag = "<$this->tag>";
        }

        if ($this->isDouble()) {

            $innerHTML = $this->innerHTML();

            if (strlen($innerHTML) > 0) {
                $render = trim($open_tag . $innerHTML . $close_tag);
            } else {
                $render = trim($open_tag . $close_tag);
            }
        } else {
            $render = $open_tag;
        }

        $render = \PiecesPHP\Core\HTML\FormatHtml::format($render);

        if ($echo) {
            echo $render;
        }

        return $render;
    }

    /**
     * clearChilds
     *
     * @return void
     */
    public function clearChilds()
    {
        $this->childs = new ElementArray();
    }

    /**
     * isDouble
     *
     * @return bool
     */
    public function isDouble(): bool
    {
        return $this->isDoubleTag;
    }

    /**
     * createAttributes
     *
     * @param array $attributes
     * @return Attribute[]
     */
    protected function createAttributes(array $attributes)
    {
        $attrs = [];
        foreach ($attributes as $attr => $value) {
            if (is_string($attr)) {
                $attrs[] = new Attribute($attr, $value);
            } else if ($value instanceof Attribute) {
                $attrs[] = $value;
            }
        }
        return $attrs;
    }

    /**
     * create
     *
     * @param string $tag
     * @param string $text
     * @param Element[]|Element $childs
     * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
     * ['attributo'=>'value']
     * o
     * ['attributo'=>['value','value2'...]]
     * @param bool $doubleTag
     * @return HtmlElement
     * @throws MalformedChildException
     */
    public static function create(string $tag, string $text = '', $childs = null, $attributes = null, bool $doubleTag = true): Element
    {
        return new HtmlElement($tag, $text, $childs, $attributes, $doubleTag);
    }

    /**
     * __toString
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render(false);
    }
}
