<?php

/**
 * Element.php
 */
namespace PiecesPHP\Core\HTML\Interfaces;


/**
 * Element - Elemento html
 * 
 * Funciona como módulo independiente
 * @category 	HTML
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 * @info Funciona como módulo independiente
 */
interface Element
{
	/**
	 * appendChild
	 *
	 * @param Element $child
	 * @return void
	 */
	public function appendChild(Element $child);

	/**
	 * setAttribute
	 *
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public function setAttribute(string $name, $value = null);

	/**
	 * getAttributes
	 *
	 * @param bool $outputAsString Si es true devuelve una cadena con los atributos, si es false;
	 * un array con cada atributo como índice y sus valores como un array.
	 * @return string|array
	 */
	public function getAttributes(bool $outputAsString = true);

	/**
	 * getAttribute
	 *
	 * @param string $name El nombre del atributo
	 * @param bool $outputAsString Si es true devuelve el valor como cadena, si es false; como un array.
	 * @return string|array  Si no existe, devuelve null
	 */
	public function getAttribute(string $name, bool $outputAsString = true);

	/**
	 * getText
	 *
	 * @return string
	 */
	public function getText(): string;

	/**
	 * getUniqueID
	 *
	 * @return string
	 */
	public function getUniqueID(): string;
	
	/**
	 * innerHTML
	 *
	 * @param mixed $html
	 * @return void|string
	 */
	public function innerHTML(Element $html = null);


	/**
	 * render
	 *
	 * @param bool $echo
	 * @return string
	 */
	public function render($echo = true):string;

	/**
	 * isDouble
	 *
	 * @return bool
	 */
	public function isDouble():bool;

	/**
	 * create
	 *
	 * @param string $tag
	 * @param string $text
	 * @param Element[]|Element $childs
	 * @param Attribute|array $attributes Objeto Attribute o un array asociativo como el siguiente:
	 * ['attributo'=>'value']
	 * o
	 * ['attributo'=>['value','value2'...]]
	 * @param bool $doubleTag
	 * @return Element
	 */
	public static function create(string $tag, string $text = '', $childs = null, $attributes = null, bool $doubleTag = true):Element;
}
