<?php

/**
 * MalformedAttributeException.php
 */
namespace PiecesPHP\Core\HTML\Exceptions;

/**
 * MalformedAttributeException - ....
 * 
 * @category 	Exceptions
 * @package     PiecesPHP\Core\HTML\Exceptions
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1
 * @copyright   Copyright (c) 2018
 */
class MalformedAttributeException extends \Exception
{
	/**
	 * __construct
	 *
	 * @param \Throwable $previous
	 */
	public function __construct(int $code = 0, \Throwable $previous = null)
	{	
		parent::__construct('malformed_attribute',$code,$previous);
	}
}
