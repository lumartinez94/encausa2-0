<?php

/**
 * DatabaseClassesExceptions.php
 */

namespace PiecesPHP\Core\Database\Exceptions;

use PiecesPHP\Core\Database\Enums\CodeStringExceptionsEnum;

/**
 * DatabaseClassesExceptions - Base de las excepciones
 *
 * @package     PiecesPHP\Core\Database\Exceptions
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2019
 */
class DatabaseClassesExceptions extends \Exception
{

    protected $codeString = CodeStringExceptionsEnum::Exception;

    /**
     * __construct
     *
     * @param string $message
     * @param string $codeString
     * @param int $code
     * @return static
     */
    public function __construct(string $message, string $codeString = null, int $code = 0)
    {

        parent::__construct($message, $code);

        $codeString = !is_null($codeString) ? trim($codeString) : null;
        $this->codeString = !is_null($codeString) && strlen($codeString) > 0 ? $codeString : $this->codeString;

    }

    /**
     * getCodeString
     *
     * @return string
     */
    public final function getCodeString()
    {
        return $this->codeString;
    }

}
