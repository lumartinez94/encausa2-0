<?php

/**
 * EntityMapper.php
 */
namespace PiecesPHP\Core\Database;

use PiecesPHP\Core\Database\Enums\CodeStringExceptionsEnum;
use PiecesPHP\Core\Database\Exceptions\DatabaseClassesExceptions;

/**
 * EntityMapper - Implementación básica de un ORM
 *
 *
 * Constituye una abstracción de una entidad/tabla.
 *
 * @todo        Crear tablas automáticamente
 * @todo        Agregar diferentes relaciones
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class EntityMapper implements \JsonSerializable, \Serializable
{
    /**
     * $table
     *
     * @var string La tabla en la base de datos
     */
    protected $table = null;

    /**
     * $fields
     *
     * @var array Los campos en la base de datos con sus configuraciones
     */
    protected $fields = null;

    /**
     * $fieldsNames
     *
     * @var array Los nombres de los campos en la base de datos
     */
    protected $fieldsNames = null;

    /**
     * $fieldsNamesOnSelect
     *
     * @var array Los nombres de los campos usados en la consulta SELECT
     */
    protected $fieldsNamesOnSelect = null;

    /**
     * $primaryKey
     *
     * @var string Nombre de la llave primaria
     */
    protected $primaryKey = '';

    /**
     * $defaultFieldOptionsValues
     *
     * @var array Los valores por defecto de las opciones de campos
     */
    protected $defaultFieldOptionsValues = [
        'type' => 'text',
        'length' => 0,
        'null' => false,
        'empty' => true,
        'default' => null,
        'raw' => false,
        'primary_key' => false,
        'auto_increment' => false,
        'reference_table' => null,
        'reference_field' => null,
        'reference_primary_key' => null,
        'human_readable_reference_field' => null,
        'has_many' => false,
        'mapper' => '\stdClass',
        'representation_on_select_statement' => null,
        'meta' => false,
    ];

    /**
     * $defaultFieldOptionsComments
     *
     * @var array Comentarios de la utilidad de cada opción de los campos
     */
    protected $defaultFieldOptionsComments = [
        'type' => 'El tipo (SQL) del campo. Para ver los tipos use la propiedad supportedTypes o supportedTypesComments para información sobre ellos.',
        'length' => 'Longitud usada para validar las entradas. Para ver que tipos son validados use la propiedad typesValidateLength.',
        'null' => 'Define si el campo puede o no ser nulo.',
        'empty' => 'Define si el campo puede o no se un array/string vacío.',
        'default' => 'Define un valor por defecto.',
        'raw' => 'Define si el mapeador relizará conversión de tipos antes de almacenar la información en base de datos.',
        'primary_key' => 'Define si el campo es llave primaria.',
        'auto_increment' => 'Define si el campo es autoincrementable. (Solo las llaves primarias).',
        'reference_table' => 'Define si el campo hace rerencia a otra tabla.',
        'reference_field' => 'Define el campo asocicado en la tabla de referencia.',
        'reference_primary_key' => 'Define la llave primaria de la tabla de referencia. (Por defecto reference_field).',
        'human_readable_reference_field' => 'Define el campo de la tabla de referencia usado para visualizar con el método humanReadable()',
        'has_many' => 'Define si la relación que tiene con la otra tabla es de uno a muchos o uno a uno.',
        'mapper' => 'Define como se representarán los valores de la tabla asociadas, por defecto es un objecto stdClass, pero acepta instancias de EntityMapper. Debe ser el nombre calificado de la clase.',
        'representation_on_select_statement' => 'En caso de que el campo no exista realmente en la tabla está opción se usa para definir campos compuestos en la consulta SQL.',
        'meta' => 'Define si el campo existe o no en la tabla.',
        'NOTA' => 'Para ver el tipo de valor aceptado en cada opción use la propiedad defaultFieldOptionsValues',
    ];

    /**
     * $fieldOptionsStructure
     *
     * @var array Los tipos de las opciones de los campos
     */
    protected $fieldOptionsStructure = [
        'type' => 'string',
        'length' => ['int', 'null'],
        'null' => 'bool',
        'empty' => 'bool',
        'default' => 'mixed',
        'raw' => 'bool',
        'primary_key' => 'bool',
        'auto_increment' => 'bool',
        'reference_table' => ['string', 'null'],
        'reference_field' => ['string', 'null'],
        'reference_primary_key' => ['string', 'null'],
        'human_readable_reference_field' => ['string', 'null'],
        'has_many' => 'bool',
        'mapper' => ['string', 'null'],
        'representation_on_select_statement' => ['string', 'null'],
        'meta' => ['bool', 'null'],
    ];

    /**
     * $onlySupportedTypes
     *
     * @var bool Restringe los tipos de campos a solo los soportados
     */
    protected $onlySupportedTypes = false;

    /**
     * $supportedTypes
     *
     * @var array Los tipos SQL soportados
     */
    protected $supportedTypes = [
        'varchar',
        'text',
        'mediumtext',
        'longtext',
        'int',
        'float',
        'double',
        'json',
        'datetime',
        'date',
        'serialized_object',
    ];

    /**
     * $supportedTypesComments
     *
     * @var array Comentarios de la utilidad de cada tipo SQL soportado
     */
    protected $supportedTypesComments = [
        'varchar' => 'Acepta strings',
        'text' => 'Acepta strings',
        'int' => 'Acepta enteros o datos que pueden ser convertidos a enteros.',
        'float' => 'Acepta decimales o datos que pueden ser convertidos a decimales.',
        'double' => 'Acepta decimales o datos que pueden ser convertidos a decimales.',
        'json' => 'No es un tipo SQL, sino una opción para trabajar con campos que dentro de PHP se quieren usar como objetos. Acepta strings que seas JSON válidos, objetos y arrays.',
        'datetime' => 'Acepta strings de fechas que puedan ser interpretados por \DateTime e instancias de \DateTime',
        'date' => 'Acepta strings de fechas que puedan ser interpretados por \DateTime e instancias de \DateTime',
    ];

    /**
     * $typesValidateLength
     *
     * @var array Los tipos SQL a los que se valida la longitud
     */
    protected $typesValidateLength = ['varchar', 'text'];

    /**
     * $foreingsKeys
     *
     * @var array Nombres de las llaves foráneas
     */
    private $foreingsKeys = [];

    /**
     * $publicStructureMapperConfig
     *
     * @var array La estructura del objeto
     */
    private $publicStructureMapperConfig = [
        'table' => 'string',
    ];

    /**
     * $model
     *
     * @var ActiveRecordModel El modelo
     */
    private $model = null;

    /**
     * $properties
     *
     * @var array Propiedades agregadas dinámicamente
     */
    private $properties = [];

    /**
     * $onlyReadFields
     *
     * @var array Propiedades de solo lectura
     */
    private $onlyReadFields = [
        'primaryKey',
        'defaultFieldOptionsValues',
        'defaultFieldOptionsComments',
        'fieldOptionsStructure',
        'supportedTypes',
        'supportedTypesComments',
        'typesValidateLength',
        'model',
    ];

    /**
     * $optionsInstanceDB
     *
     * @var array Opciones de configuración del modelo
     */
    private $optionsInstanceDB = [];

    /**
     * $optionsDB
     *
     * @var array Opciones de configuración del modelo
     */
    protected static $optionsDB = [];

    /**
     * $escapeOpenCharacter
     *
     * @var string Carácter para espacar nombres de columnas
     */
    protected static $escapeOpenCharacter = "`";

    /**
     * $escapeCloseCharacter
     *
     * @var string Carácter para espacar nombres de columnas
     */
    protected static $escapeCloseCharacter = "`";

    /**
     * $thosandsSeparator
     *
     * @var string $thosandsSeparator
     */
    private static $thosandsSeparator = '.';

    /**
     * $decimalsSeparator
     *
     * @var string $decimalsSeparator
     */
    private static $decimalsSeparator = ',';

    /**
     * __construct
     *
     * @param mixed $value_compare Debe ser un tipo escalar
     * @param string $field_compare
     * @param array $options Las opciones de configuración
     * Opciones aceptadas:
     * - driver (string) El controlador PDO.  Opcional. Por defecto mysql
     * - database (string) Nombre de la base de datos. Obligatorio
     * - host (string) Servidor. Opcional. Por defecto localhost
     * - user (string) Usuario. Opcional. Por defecto root
     * - password (string) Contraseña. Opcional. Por defecto una cadena vacía
     * - charset (string) El juego de caracteres. Opcional. Por defecto utf8
     * Nota: esto sobreescribe a las opciones pasadas por la función estática setOptions
     * @return static
     */
    public function __construct($value_compare = null, string $field_compare = 'primary_key', $options = null)
    {
        if (is_array($options)) {
            $this->optionsInstanceDB = $options;
        }

        if (count($this->optionsInstanceDB) > 0) {
            $options = $this->optionsInstanceDB;
        } else if (count(static::$optionsDB) > 0) {
            $options = static::$optionsDB;
        }

        $this->optionsInstanceDB = $options;

        $this->model = new ActiveRecordModel($options);
        $this->model->setTable($this->table);

        //Comprobar si la tabla está definida
        if (!is_string($this->table) || strlen($this->table) == 0) {
            throw new DatabaseClassesExceptions('No hay ninguna tabla definida.', CodeStringExceptionsEnum::UndefinedTable);
        }

        //Comprobar si hay campos
        if (!is_array($this->fields) || count($this->fields) == 0) {
            throw new DatabaseClassesExceptions('No ha ningún campo definido.', CodeStringExceptionsEnum::UndefinedField);
        }

        //Validar la propiedad fields
        $this->fields = $this->validateFields($this->fields);

        //Establecer un array con los nombres de los campos
        $this->fieldsNames = array_keys($this->fields);

        //Establecer un array con los nombres de los tal como la opción representation_on_select_statement
        $this->fieldsNamesOnSelect = array_values(array_map(function ($e) {
            return $e['representation_on_select_statement'];
        }, $this->fields));

        $field_compare = $field_compare == 'primary_key' ? $this->primaryKey : $field_compare;

        //Comprobar que el valor de comparación para la instancia sea escalar
        if (!is_null($value_compare) && !is_scalar($value_compare)) {
            throw new DatabaseClassesExceptions('El parámetro $value_compare debe ser de tipo escalar.', CodeStringExceptionsEnum::TypeError);
        }

        if (!is_null($value_compare)) {

            $result = $this->getValues([$field_compare => $value_compare]);
            if (is_array($result) && count($result) > 0) {
                $first_result = $result[0];
                foreach ($first_result as $property => $value) {
                    $this->$property = $value;
                }
            }
        }
    }

    /**
     * save
     *
     * @return bool
     */
    public function save()
    {
        $data = [];
        foreach ($this->fields as $name => $options) {

            $info_field = $this->fields[$name];
            $primary_key = $info_field['primary_key'];
            $default = $info_field['default'];
            $type = $info_field['type'];
            $reference_table = $info_field['reference_table'];
            $meta = $info_field['meta'];

            if ($this->$name === null) {
                if ($primary_key !== true) {
                    $this->$name = $default;
                }
            }

            if ($this->$name !== null) {

                if ($meta) {
                    continue;
                }

                if ($reference_table !== null) {
                    $reference_field = $info_field['reference_field'];
                    $mapper = $info_field['mapper'];
                    if ($this->$name instanceof $mapper) {
                        $data[$name] = $this->$name->$reference_field;
                    } else {
                        $data[$name] = self::castPHPToSQLTypes($type, $this->$name);
                    }
                } else {
                    $data[$name] = self::castPHPToSQLTypes($type, $this->$name);
                }
            }
        }
        $primary_key = $this->primaryKey;
        unset($data[$primary_key]);
        if (!empty($this->$primary_key)) {
            return $this->update();
        } else {
            return $this->model->insert($data)->execute();
        }
    }

    /**
     * update
     *
     * @return bool
     */
    public function update()
    {
        $data = [];
        $name_primary_key = '';

        foreach ($this->fields as $name => $options) {
            if ($options['primary_key']) {
                $name_primary_key = $name;
            }

            $can_be_null = $options['null'];
            $valid_for_update = $name != $name_primary_key; //Que no sea la llave primaria

            if (!$can_be_null) {
                $valid_for_update = $valid_for_update && $this->$name !== null; //Que no sea nulo
            }

            if ($valid_for_update) {

                $info_field = $this->fields[$name];
                $type = $info_field['type'];
                $reference_table = $info_field['reference_table'];
                $meta = $info_field['meta'];

                if ($meta) {
                    continue;
                }

                if ($reference_table !== null) {
                    $reference_field = $info_field['reference_field'];
                    $mapper = $info_field['mapper'];
                    if ($this->$name instanceof $mapper) {
                        $data[$name] = $this->$name->$reference_field;
                    } else {
                        $data[$name] = self::castPHPToSQLTypes($type, $this->$name);
                    }
                } else {
                    $data[$name] = self::castPHPToSQLTypes($type, $this->$name);
                }
            }
        }
        return $this->model->update($data)->where([$name_primary_key => $this->$name_primary_key])->execute();
    }

    /**
     * getModel
     *
     * @return ActiveRecordModel
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * getLastInsertID
     *
     * @return int
     */
    public function getLastInsertID()
    {
        return $this->model->lastInsertId();
    }

    /**
     * setOptions
     *
     * @param array $options
     * @return void
     */
    public static function setOptions(array $options)
    {
        static::$optionsDB = $options;
    }

    /**
     * getFields
     *
     * @return array
     */
    public static function getFields()
    {
        return (new static())->fields;
    }

    /**
     * getPrimaryKey
     *
     * @return string
     */
    public static function getPrimaryKey()
    {
        return (new static())->primaryKey;
    }

    /**
     * __get
     *
     * @param mixed $name
     * @return mixed
     */
    public function __get($name)
    {
        $classname = get_class($this);

        //-----------Comprobar si la propiedad es privada

        $excludes = $this->onlyReadFields;
        $reflection = new \ReflectionClass($classname);
        $privateProperties = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE);
        foreach ($privateProperties as $property) {
            if ($property->name == $name && !in_array($property->name, $excludes)) {
                throw new DatabaseClassesExceptions("La propiedad $classname::$name es privada", CodeStringExceptionsEnum::PrivateProperty);
            }
        }
        $reflection = new \ReflectionClass(self::class);
        $privateProperties = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE);
        foreach ($privateProperties as $property) {
            if ($property->name == $name && !in_array($property->name, $excludes)) {
                throw new DatabaseClassesExceptions("La propiedad $classname::$name es privada", CodeStringExceptionsEnum::PrivateProperty);
            }
        }

        //-----------Comprobar si la propiedad es privada FIN
        if (array_key_exists($name, $this->properties) || in_array($name, $this->fields) || array_key_exists($name, $this->fields)) {
            //Comprueba si es una propiedad definida en la clase

            if (in_array($name, $this->fields) || array_key_exists($name, $this->fields)) {

                $field_info = $this->fields[$name];
                $reference_table = $field_info['reference_table'];
                $meta = $field_info['meta'];
                $type = strtolower($field_info['type']); //Tipo
                $value = null;
                if ($meta && !isset($this->properties[$name])) {
                    return null;
                }
                if (!isset($this->properties[$name])) {
                    return null;
                }
                if ($reference_table !== null) {
                    $value = $this->properties[$name];
                } else {
                    $value = self::castPHPToSQLTypes($type, $this->properties[$name], true);
                }

                return $value;
            } else {
                return $this->properties[$name];
            }
        } else if (in_array($name, $this->onlyReadFields)) {
            return $this->$name;
        } else {
            throw new DatabaseClassesExceptions("La propiedad $classname::$name no está definida.", CodeStringExceptionsEnum::UndefinedProperty);
        }
    }

    /**
     * __set
     *
     * @param mixed $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value)
    {
        $classname = get_class($this);

        if (in_array($name, $this->onlyReadFields)) {
            throw new DatabaseClassesExceptions("La propiedad $classname::$name es de solo lectura.", CodeStringExceptionsEnum::OnlyReadProperty);
        }

        /**
         * Verificar si es una de las propiedades restringidas de la clase
         * y validar los tipos
         */
        if (in_array($name, $this->publicStructureMapperConfig)) {
            $value = $this->validateMapperBaseStructure($name, $value);
        }

        //-----------Comprobar si la propiedad es privada
        $reflection = new \ReflectionClass($classname);
        $privateProperties = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE);
        foreach ($privateProperties as $property) {
            if ($property->name == $name) {
                throw new DatabaseClassesExceptions("La propiedad $classname::$name es privada", CodeStringExceptionsEnum::PrivateProperty);
            }
        }
        //-----------Comprobar si la propiedad es privada FIN

        $valid_property = false;

        if (array_key_exists($name, $this->properties) || in_array($name, $this->fields) || array_key_exists($name, $this->fields)) {
            //Comprueba si es una propiedad definida en la clase
            if (in_array($name, $this->fields) || array_key_exists($name, $this->fields)) {
                //Validar el valor del campo
                $this->properties[$name] = $this->validateFieldValue($name, $value);
                $valid_property = true;
            } else {
                $this->properties[$name] = $value;
                $valid_property = true;
            }
        }

        if (!$valid_property) {
            throw new DatabaseClassesExceptions("La propiedad $classname::$name no está definida.", CodeStringExceptionsEnum::UndefinedProperty);
        }
    }

    /**
     * getAll
     *
     * @return static[]
     */
    public function getAll()
    {
        $result = $this->getValues();
        $results = [];
        foreach ($result as $e) {
            $instance = new static();
            foreach ($e as $property => $value) {
                $instance->$property = $value;
            }
            $results[] = $instance;
        }
        return $results;
    }

    /**
     * getValues
     *
     * Obtiene los valores de los campos
     *
     * @param array|string $where
     * @return array
     */
    private function getValues($where = null)
    {
        $result = [];

        if (count($this->foreingsKeys) > 0) {
            $result = $this->getForeingQuery($where);
        } else {
            $query = $this->model->select($this->fieldsNamesOnSelect);

            if (!is_null($where)) {
                $query = $query->where($where);
            }

            $result = $query->execute();
            $result = $query->result();
        }
        $this->model->resetAll();
        return $result;
    }

    /**
     * getForeingQuery
     *
     * Establece los valores de los campos y remplaza el valor de las claves foráneas
     * con el objeto relacionado al/los registro/s de la tabla asociada
     *
     * @param array|string $where
     * @return array
     */
    private function getForeingQuery($where = null)
    {
        $result = []; //Resultados
        $query = ''; //Consulta
        $tables_joining = []; //Tablas asociadas
        $select_fields = $this->fieldsNamesOnSelect; //Array con los campos del SELECT
        $foreing_keys = $this->foreingsKeys; //Llaves foránes

        //Se recorren las llaves foráneas para establecer información relevante
        foreach ($foreing_keys as $name) {

            $info = $this->fields[$name]; //Información de la llave foránea

            $table = $info['reference_table']; //Tabla referenciada
            $field = $info['reference_field']; //Campo al que se referencia

            $qualified_field_name = "$table.$field"; //Nombre completo del campo referenciado

            //Se establece la información relevante
            $tables_joining[] = [
                'table' => $table,
                'field_compare' => $qualified_field_name,
                'key' => $name,
                'info_key' => $info,
            ];
        }

        //Consulta base
        $query = $this->model->select($select_fields);

        //Se establecen condiciones en caso de haber
        if (!is_null($where)) {
            $query = $query->where($where);
        }

        //Se hace la consulta de la tabla principal
        $result = $query->execute();
        $result = $query->result();

        //Si hay resultados
        if (is_array($result) && count($result) > 0) {

            //Se recorren los resultados para asociarlos con sus referencias
            foreach ($result as $pos => $element) {

                //Se recorren las tablas referenciadas
                foreach ($tables_joining as $index => $field) {

                    $table = $field['table']; //Tabla referenciada
                    $field_compare = $field['field_compare']; //Campo de la tabla referenciada
                    $key = $field['key']; //Campo de la tabla de la entidad
                    $info_key = $field['info_key']; //Información de la llave foránea de la entidad
                    $has_many = $info_key['has_many']; //Verifica si espera uno o múltiples resultados
                    $mapper = $info_key['mapper']; //Tipo de mapper de la tabla asociada
                    $reference_primary_key = $info_key['reference_primary_key']; //Llave primaria de la tabla referenciada

                    $model = new ActiveRecordModel($this->optionsInstanceDB); //Se crea un modelo

                    $model->setTable($table); //Se establece la tabla referenciada al modelo

                    $query = $model->select(); //Consulta base

                    if (is_null($element->$key)) {
                        continue;
                    }

                    $query = $query->where([ //Condición de la consulta
                        $field_compare => $element->$key,
                    ]);

                    $value = $query->execute(); //Se ejecuta la consulta
                    $value = $query->result();

                    if ($has_many) {
                        //Si se esperan varios resultados
                        if (is_array($value)) {
                            //Si es un array
                            $result[$pos]->$key = $value;
                        } else {
                            //False si hubo algún error
                            $result[$pos]->$key = false;
                        }
                    } else {
                        if (is_array($value)) {
                            //Si es un array
                            if (count($value) > 0) {
                                //Si hay elementos
                                $value = $value[0];
                                if ($mapper != '\stdClass' && class_exists($mapper)) {
                                    if ($reference_primary_key === null) {
                                        if (method_exists($mapper, 'getPrimaryKey')) {
                                            $reference_primary_key = $mapper::getPrimaryKey();
                                            $value = new $mapper($value->$reference_primary_key);
                                        }
                                    } else {
                                        $value = new $mapper($value->$reference_primary_key);
                                    }
                                }
                                $result[$pos]->$key = $value; //Se asigna solo el primer elemento
                            } else {
                                $result[$pos]->$key = null; //Null si no hay elementos
                            }
                        } else {
                            //False si hubo algún error
                            $result[$pos]->$key = false;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * validateMapperBaseStructure
     *
     * Valida la estructura básica de la clase
     *
     * Devuelve el valor con correcto
     *
     * @param string $name El nombre de la propiedad
     * @param mixed $value El valor a validar
     * @return void
     * @throws DatabaseClassesExceptions
     */
    private function validateMapperBaseStructure(string $name, $value)
    {
        $type = $this->publicStructureMapperConfig[$name];
        $classname = get_class($this);

        if ($name == 'fields') {
            $value = $this->validateFields($value);
        } else {
            if (!self::validateType($type, $value)) {
                throw new DatabaseClassesExceptions(
                    "La propiedad $classname::$name debe ser de tipo: " . $this->publicStructureMapperConfig[$name],
                    CodeStringExceptionsEnum::InvalidTypeValueProperty
                );
            }
        }
        return $value;
    }

    /**
     * validateFieldValue
     *
     * Valida la entrada y devuelve el valor
     *
     * @param string $fieldName
     * @param mixed $value
     * @return mixed
     */
    private function validateFieldValue(string $fieldName, $value)
    {
        $field_info = $this->fields[$fieldName];

        $primary_key = $field_info['primary_key']; //Si es llave primaria : bool
        $auto_increment = $field_info['auto_increment']; //AUTO_INCREMENT

        $reference_table = $field_info['reference_table']; //Nombre de la tabla de referencia : string|null
        $reference_field = $field_info['reference_field']; //Nombre del campo de referencia : string|null
        $mapper = $field_info['mapper']; //Nombre del modelo de entidad : string|null

        $null = $field_info['null']; //Si puede ser o no nulo : bool

        $empty = $field_info['empty']; //Si un string o array vacío : bool

        $meta = $field_info['meta']; //Si el campo no existe en la tabla : bool
        if ($meta) {
            $null = true;
        }

        $default = $field_info['default']; //Valor por defecto : mixed|null
        if (is_string($default)) {
            $default = trim($default);
            switch ($default) {
                case 'timestamp':
                    $default = new \DateTime();
                    break;
                case 'date':
                    $default = new \DateTime();
                    break;
                default:
                    $default = $default;
                    break;
            }
        }

        $length = $field_info['length']; //Longitud : int|null

        $type = strtolower($field_info['type']); //Tipo SQL : string

        //Verificar que el tipo esté soportado
        if ($this->onlySupportedTypes && !in_array($type, $this->supportedTypes)) {
            throw new DatabaseClassesExceptions(
                "El tipo $type no está soportado, los tipos soportados son " . implode('|', $this->supportedTypes) . ".",
                CodeStringExceptionsEnum::InvalidTypeValueField
            );
        }

        $raw = $field_info['raw']; //Si debe hacer conversión de tipos antes de guardar en la base de datos : bool

        //Comprueba si no acepta nulos
        if (!$null) {

            $nulo = is_null($value);

            if ($nulo && is_null($default)) {
                //Si el valor ingresado es null
                if ($primary_key) {
                    return null;
                }
                throw new DatabaseClassesExceptions("El campo $fieldName no acepta valores nulos.", CodeStringExceptionsEnum::NotAllowedNullField);
            } else if ($nulo) {
                //Si el valor ingresado es null se le asigna el valor por defecto
                $value = $default;
            }
        }

        //Comprueba si valores vacíos
        if (!$empty) {

            $is_empty = false;
            $is_empty = $is_empty || (is_string($value) && strlen($value) == 0);
            $is_empty = $is_empty || (is_array($value) && count($value) == 0);

            if ($nulo && is_null($default)) {
                //Si el valor ingresado es vacío
                if ($primary_key) {
                    return null;
                }
                throw new DatabaseClassesExceptions("El campo $fieldName no acepta valores vacíos.", CodeStringExceptionsEnum::NotAllowedEmptyField);
            } else if ($nulo) {
                //Si el valor ingresado es vacío se le asigna el valor por defecto
                $value = $default;
            }
        }

        if (!self::validateType($type, $value) && $reference_table === null) {
            $value = $default;
        }

        //Comprueba la longitud del valor solo en algunos casos
        $types_validate_length = $this->typesValidateLength;
        if (!$null && ($length !== null && in_array($type, $types_validate_length))) {
            if (!is_scalar($value) || (strlen((string) $value) > $length && $length != 0)) {
                throw new DatabaseClassesExceptions("El campo $fieldName no puede superar la longitud $length", CodeStringExceptionsEnum::NotValidLengthField);
            }
        }

        //Verifica si se hará conversión de tipos
        if ($raw === false) {
            if ($reference_table === null) {
                $value = self::castPHPToSQLTypes($type, $value);
            } else {
                $mapper = class_exists($mapper) ? $mapper : $this->defaultFieldOptionsValues['mapper'];
                if (!($value instanceof $mapper)) {
                    $value = self::castPHPToSQLTypes($type, $value);
                }
            }
        }

        //Valida el tipo
        $can_not_null_is_null = !$null && is_null($value);
        $can_null_is_not_null = $null && !is_null($value);
        $can_not_null_is_not_null = !$null && !is_null($value);
        $invalid_type = !self::validateType($type, $value);
        if ($can_not_null_is_null || $invalid_type) {
            if ($reference_table === null) {
                if ($invalid_type) {
                    if ($can_not_null_is_null) {
                        throw new DatabaseClassesExceptions(
                            "El campo $fieldName no es compatible con el tipo: " . gettype($value),
                            CodeStringExceptionsEnum::InvalidTypeValueField
                        );
                    }
                    if ($can_null_is_not_null) {
                        throw new DatabaseClassesExceptions(
                            "El campo $fieldName no es compatible con el tipo: " . gettype($value),
                            CodeStringExceptionsEnum::InvalidTypeValueField
                        );
                    }
                    if ($can_not_null_is_not_null) {
                        throw new DatabaseClassesExceptions(
                            "El campo $fieldName no es compatible con el tipo: " . gettype($value),
                            CodeStringExceptionsEnum::InvalidTypeValueField
                        );
                    }
                }
            } else {
                $mapper = class_exists($mapper) ? $mapper : $this->defaultFieldOptionsValues['mapper'];
                if ($can_null_is_not_null) {
                    if (is_array($value)) {
                        foreach ($value as $i) {
                            if (!($i instanceof $mapper) && !self::validateType($type, $value)) {
                                throw new DatabaseClassesExceptions(
                                    "El campo $fieldName no es compatible con el tipo: " . gettype($value),
                                    CodeStringExceptionsEnum::InvalidTypeValueField
                                );
                            }
                        }
                    } else {
                        if (!($value instanceof $mapper) && !self::validateType($type, $value)) {
                            throw new DatabaseClassesExceptions(
                                "El campo $fieldName no es compatible con el tipo: " . gettype($value),
                                CodeStringExceptionsEnum::InvalidTypeValueField
                            );
                        }
                    }
                }
            }
        }

        return $value;
    }

    /**
     * validateFields
     *
     * Valida la estructura de $fields
     *
     * Devuelve el valor con correcto
     *
     * @param mixed $value
     * @return mixed
     * @throws DatabaseClassesExceptions
     */
    private function validateFields($value)
    {
        $this->primaryKey = '';
        $this->foreingsKeys = [];
        //Valores por defecto de las opciones
        $defaults = $this->defaultFieldOptionsValues;

        //Si existe alguna llave primaria
        $has_primary_key = false;

        //Las opciones de los campos y sus tipos
        $options = $this->fieldOptionsStructure;

        //Valor para validar
        $fields = $value;

        //Comprueba que $fields sea un array
        if (!self::validateType('array', $fields)) {
            throw new DatabaseClassesExceptions(get_class($this) . '::fields debe ser un array.');
        }

        //Recorre $fields para validar los valores
        foreach ($fields as $field => $config) {

            if (self::validateType('string', $field)) {
                //Validaciones en caso de que la llave sea un string

                //Comprueba que el $config de sea un array (puesto que son las diferentes opciones del campo)
                if (!self::validateType('array', $config)) {
                    throw new DatabaseClassesExceptions('La configuración del campo debe ser un array.');
                }

                //Recorre las opciones para verificar que los valores en $config sean de tipos correctos
                foreach ($options as $name => $types) {

                    //Vuelve $types un array, en caso de no serlo, para más comodidad
                    $types = is_array($types) ? $types : [$types];

                    //Comprueba si la opción fue suministrada
                    if (array_key_exists($name, $config)) {

                        //Validez del valor de la opción
                        $valid_option_value = true;

                        //Recorre los tipos aceptados por la opción para validar la entrada
                        foreach ($types as $type) {
                            //Comprueba que la entrada sea de un tipo permitido
                            if (self::validateType($type, $config[$name])) {
                                $valid_option_value = true;
                                break;
                            } else {
                                $valid_option_value = false;
                            }
                        }

                        //Si el valor de la opción fue inválido
                        if (!$valid_option_value) {
                            throw new DatabaseClassesExceptions("La opción $name debe ser tipo: " . implode('|', $types));
                        }
                    } else {
                        //Se asigna un valor por defecto a la opción, que no fue suministrada
                        $fields[$field][$name] = $defaults[$name];
                    }
                }

                //Asigna valores en las opciones en función de los predeterminados

                //------representation_on_select_statement
                if ($fields[$field]['representation_on_select_statement'] !== null && $fields[$field]['meta']) {

                    $escaped_field = self::$escapeOpenCharacter . $field . self::$escapeCloseCharacter;

                    $fields[$field]['representation_on_select_statement'] .= " AS $escaped_field";

                } else {

                    $escaped_field = self::$escapeOpenCharacter . $field . self::$escapeCloseCharacter;

                    $fields[$field]['representation_on_select_statement'] = $escaped_field;

                }

            } else if (self::validateType('int', $field)) {
                //Validaciones en caso de que la llave sea un entero

                if (!self::validateType('string', $config)) {
                    throw new DatabaseClassesExceptions('El nombre del campo debe ser un string.');
                }
            } else {
                //Si la llave no es entero ni string
                throw new DatabaseClassesExceptions('Hay error en: ' . get_class($this) . '::fields.');
            }
        }

        //Recorre $fields para validar incongruencias
        foreach ($fields as $field => $config) {
            //Comprueba si el campo es llave primaria
            if ($config['primary_key']) {
                //Comprueba si ya existe una llave primaria
                if ($has_primary_key) {
                    throw new DatabaseClassesExceptions('No puede haber más de una llave primaria.');
                } else {
                    //Establece el nombre de la llave primaria
                    $this->primaryKey = $field;
                    $has_primary_key = true;
                }
            }
            //Comprueba si el campo tiene auto incremento
            if ($config['auto_increment']) {
                //Comprueba si no es llave primaria
                if (!$config['primary_key']) {
                    throw new DatabaseClassesExceptions('Solo los campos de tipo PRIMARY KEY puden ser auto incrementables.');
                }
            }
            //Comprueba si el campo es llave foránea
            if ($config['reference_table'] || $config['reference_field']) {
                //Agrega la llave foránea
                $this->foreingsKeys[] = $field;
                $table = $config['reference_table'];
                //Comprueba si hay una tabla de referencia definida
                if (!self::validateType('string', $config['reference_table'])) {
                    throw new DatabaseClassesExceptions("Debe definir la tabla de referencia para el campo foráneo: $field");
                }
                //Comprueba si hay un campo de referencia definido
                if (!self::validateType('string', $config['reference_field'])) {
                    throw new DatabaseClassesExceptions("Debe definir un campo de referencia en la tabla $table para el campo foráneo: $field");
                }
            }
            //Comprueba si el campo es varchar
            if ($config['type'] == 'varchar') {
                $length = $config['length'];
                //Comprueba si hay una tabla de referencia definida
                if (is_null($length)) {
                    throw new DatabaseClassesExceptions("El tipo VARCHAR debe tener una longitud definida por la opción: length");
                }
            }
        }

        //Comprueba si no hay llaves primaria
        if (!$has_primary_key) {
            throw new DatabaseClassesExceptions('Debe haber al menos una llave primaria');
        }

        return $fields;
    }

    /**
     * jsonSerialize
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $data = [];
        foreach ($this->fields as $field => $config) {
            $data[$field] = $this->$field;
        }
        $data['table'] = $this->table;
        $data['primaryKey'] = $this->primaryKey;
        $data['foreingsKeys'] = $this->foreingsKeys;
        $data['fields'] = $this->fields;
        return $data;
    }

    public function __toString()
    {
        $data = [];
        foreach ($this->fields as $field => $config) {
            $data[$field] = $this->$field;
        }
        $data['table'] = $this->table;
        $data['defaultFieldOptionsValues'] = $this->defaultFieldOptionsValues;
        $data['defaultFieldOptionsComments'] = $this->defaultFieldOptionsComments;
        $data['fieldOptionsStructure'] = $this->fieldOptionsStructure;
        $data['supportedTypes'] = $this->supportedTypes;
        $data['supportedTypesComments'] = $this->supportedTypesComments;
        $data['typesValidateLength'] = $this->typesValidateLength;
        $data['publicStructureMapperConfig'] = $this->publicStructureMapperConfig;
        $data['primaryKey'] = $this->primaryKey;
        $data['foreingsKeys'] = $this->foreingsKeys;
        $data['fields'] = $this->fields;
        return json_encode($data, \JSON_PRETTY_PRINT);
    }

    /**
     * humanReadable
     *
     * Devuelve la información de la entidad legible
     *
     * @return array
     */
    public function humanReadable()
    {
        $data = [];
        foreach ($this->fields as $field => $config) {

            $type = $config['type'];
            $reference_field = $config['reference_field'];
            $human_readable_reference_field = $config['human_readable_reference_field'];

            $has_reference = $reference_field !== null;
            $has_reference_human_readable_field = $human_readable_reference_field !== null;

            $set_field = false;

            if ($has_reference) {

                if ($has_reference_human_readable_field) {

                    if (!is_scalar($this->$field)) {

                        if (isset($this->$field->$human_readable_reference_field)) {

                            $data[$field] = $this->$field->$human_readable_reference_field;
                            $set_field = true;
                        }
                    }
                }

                if (!$set_field) {
                    if (!is_scalar($this->$field)) {
                        if (isset($this->$field->$reference_field)) {
                            $data[$field] = $this->$field->$reference_field;
                        } else {
                            $data[$field] = $this->$field;
                        }
                    } else {
                        $data[$field] = $this->$field;
                    }
                    $set_field = true;
                }
            }

            if (!$set_field) {
                $data[$field] = $this->$field;
                $set_field = true;
            }

            $data[$field] = self::castPHPToSQLTypes($type, $data[$field]);
        }
        return $data;
    }

    /**
     * serialize
     *
     * @return string
     */
    public function serialize()
    {
        return base64_encode(serialize([
            $this->table,
            $this->primaryKey,
            $this->foreingsKeys,
            $this->fields,
            $this->optionsInstanceDB,
            $this->properties,
        ]));
    }

    /**
     * unserialize
     *
     * @return EntityMapper
     */
    public function unserialize($serializedString)
    {
        list(
            $this->table,
            $this->primaryKey,
            $this->foreingsKeys,
            $this->fields,
            $this->optionsInstanceDB,
            $this->properties
        ) = unserialize(base64_decode($serializedString));
    }

    /**
     * classInfo
     *
     * Devuele toda la estructura de la clase
     *
     * @param bool $json Si es true devuelve un strins JSON, de lo contrario un array
     * @return string|array
     */
    public static function classInfo(bool $json = false)
    {
        $data = [];
        $data['propiedades'] = [];
        $reflection = new \ReflectionClass(get_class(new static()));

        $data['Comentarios'] = $reflection->getDocComment();
        $data['Nombre'] = get_class(new static());
        $data['Implementa'] = $reflection->getInterfaceNames();
        $data['Hereda'] = $reflection->getParentClass();

        $privateProperties = $reflection->getProperties();
        foreach ($privateProperties as $property) {
            $info = trim(
                str_replace(
                    [
                        "$" . $property->name,
                        "\t",
                        '*',
                        "\r",
                        "\n",
                        "\\",
                        "/",
                    ],
                    '',
                    $property->getDocComment()
                )
            );
            $data['propiedades'][$property->name] = [
                'Comentarios' => $info,
                'Valor por defecto' => $reflection->getDefaultProperties()[$property->name],
            ];
        }

        if ($json) {
            return json_encode($data, \JSON_PRETTY_PRINT);
        } else {
            return $data;
        }
    }

    /**
     * validateType
     *
     * Valida el tipo
     *
     * Los tipos implementados son:
     *
     * - string
     * - text
     * - varchar
     * - mediumtext
     * - longtext
     * - number
     * - int
     * - array
     * - float
     * - double
     * - mixed
     * - bool
     * - null
     * - json
     * - datetime
     * - date
     * - serialized_object
     *
     * Nota: Si $type no concuerda con los anteriores, se asumirá que es el nombre de una clase y
     * se validará con instanceof
     *
     * @param string $type
     * @param mixed $value
     * @return bool
     */
    public static function validateType(string $type, $value)
    {
        switch ($type) {
            case 'string':
            case 'text':
            case 'varchar':
            case 'mediumtext':
            case 'longtext':
                if (is_scalar($value)) {
                    return true;
                }

                break;
            case 'number':
                if (is_numeric($value)) {
                    return true;
                }

                break;
            case 'int':
                if (is_string($value) && strlen($value) > 1) {
                    $value = trim($value);
                    if ($value[0] == '-') {
                        $value = substr($value, 1);
                    }
                }
                if (is_integer($value) || ctype_digit($value)) {
                    return true;
                }

                break;
            case 'array':
                if (is_array($value)) {
                    return true;
                }

                break;
            case 'float':
            case 'double':
                if (is_string($value) && strlen($value) > 1) {
                    $value = trim($value);
                    if ($value[0] == '-') {
                        $value = substr($value, 1);
                    }
                }
                if (is_float($value) || is_numeric($value)) {
                    return true;
                }

                break;
            case 'mixed':
                return true;
                break;
            case 'bool':
                if (is_bool($value)) {
                    return true;
                }

                break;
            case 'null':
                if (is_null($value)) {
                    return true;
                }

                break;
            case 'json':
                if (is_array($value) || is_object($value)) {
                    return true;
                } else {
                    $test_encoding = json_encode($value);
                    return json_last_error() == \JSON_ERROR_NONE;
                }
                break;
            case 'datetime':
            case 'date':
                if ($value instanceof \DateTime) {
                    return true;
                } else {
                    try {
                        (new \DateTime($value));
                        return true;
                    } catch (\Exception $e) {
                        return false;
                    }
                }
                break;
            case 'serialized_object':
                if (is_object($value) && array_key_exists('Serializable', class_implements($value))) {
                    return true;
                } elseif (is_string($value)) {

                    if (json_decode($value) !== null) {

                        $decode = json_decode($value, true);
                        $classname = isset($decode['classname']) ? $decode['classname'] : null;
                        $serialized = isset($decode['serialized']) ? $decode['serialized'] : null;

                        if (is_string($classname) && is_string($serialized)) {
                            return array_key_exists('Serializable', class_implements($classname));
                        }

                    }

                }
                break;
            default:
                if ($value instanceof $type) {
                    return true;
                }

                break;
        }
        return false;
    }

    /**
     * castPHPToSQLTypes
     *
     * Convierte el valor ingresado al tipo indicado compatible con SQL
     *
     * Implementados:
     *
     * - VARCHAR se convierte con (string)
     * - TEXT se convierte con (string)
     * - MEDIUMTEXT se convierte con (string)
     * - LONGTEXT se convierte con (string)
     * - INT se convierte con (int)
     * - DOUBLE se convierte con (float)
     * - FLOAT se convierte con (float)
     * - JSON se convierte con json_encode
     * - DATETIME Si value es \DateTime se convierte con $value->format('Y-m-d H:i:s');
     * - DATE Si value es \DateTime se convierte con $value->format('Y-m-d');
     * - SERIALIZED_OBJECT Cadena de objeto serializado se convierte con serialize()
     *
     * Nota: si el tipo no está implementado devuelve el mismo valor.
     *
     * @param string $typeSQL Tipo compatible en SQL (case-insensitive)
     * @param mixed $value Valor entrante
     * @param bool $revert Si se extrae de SQL y se convertirá al valor usado en PHP
     * @return mixed
     */
    public static function castPHPToSQLTypes(string $typeSQL, $value, bool $revert = false)
    {
        $typeSQL = strtolower($typeSQL);
        if (!$revert) {
            switch ($typeSQL) {
                case 'varchar':
                case 'text':
                case 'mediumtext':
                case 'longtext':
                    if (is_scalar($value)) {
                        $value = (string) $value;
                        $value = \stripslashes($value);
                        return $value;
                    } else {
                        if (is_object($value)) {
                            $has_to_string = method_exists(get_class($value), '__toString');
                            if ($has_to_string) {
                                $value = (string) $value;
                                $value = \stripslashes($value);
                                return $value;
                            }
                        }
                        return $value;
                    }
                    break;
                case 'int':
                    if (is_numeric($value)) {
                        return (int) $value;
                    } else {
                        return $value;
                    }
                    break;
                case 'float':
                case 'double':
                    if (is_numeric($value)) {
                        return (double) $value;
                    } else {
                        return $value;
                    }
                    break;
                case 'json':
                    if (is_string($value)) {
                        return $value;
                    } else {
                        return json_encode($value);
                    }
                    break;
                case 'datetime':
                    if ($value instanceof \DateTime) {
                        return $value->format('Y-m-d H:i:s');
                    } else {
                        return $value;
                    }
                    break;
                case 'date':
                    if ($value instanceof \DateTime) {
                        return $value->format('Y-m-d');
                    } else {
                        return $value;
                    }
                    break;
                case 'serialized_object':
                    if (is_object($value) && array_key_exists('Serializable', class_implements($value))) {

                        $value = [
                            'classname' => get_class($value),
                            'serialized' => $value->serialize(),
                        ];

                        return json_encode($value);

                    } else {
                        return $value;
                    }

                    break;
                default:
                    return $value;
                    break;
            }
        } else {
            switch ($typeSQL) {
                case 'varchar':
                case 'text':
                case 'mediumtext':
                case 'longtext':
                    return is_string($value) ? \stripslashes($value) : $value;
                    break;
                case 'json':
                    if (is_string($value)) {

                        $decoded_value = json_decode($value);

                        if (json_last_error() == \JSON_ERROR_NONE) {
                            return $decoded_value;
                        } else {
                            return $value;
                        }

                    } else {
                        return $value;
                    }
                    break;
                case 'datetime':
                    if (!is_null($value)) {
                        if (is_string($value)) {
                            if (strlen(trim($value)) > 0) {
                                $value = new \DateTime($value);
                            }
                        }
                    }
                    return $value;
                    break;
                case 'date':
                    if (!is_null($value)) {
                        if (is_string($value)) {
                            if (strlen(trim($value)) > 0) {
                                $value = new \DateTime($value);
                            }
                        }
                    }
                    return $value;
                    break;
                case 'serialized_object':

                    if (is_string($value)) {

                        if (is_array(json_decode($value, true))) {

                            $_value = json_decode($value, true);
                            $classname = isset($_value['classname']) ? $_value['classname'] : null;
                            $serialized = isset($_value['serialized']) ? $_value['serialized'] : null;

                            if (is_string($classname) && is_string($serialized)) {
                                $serialized_length = strlen($serialized);
                                $classname_length = strlen($classname);
                                $serialized_content = $serialized;
                                $serialized = "C:$classname_length:\"$classname\":$serialized_length:{" . $serialized_content . "}";
                                $value = unserialize($serialized);
                            }

                        }

                        return $value;

                    } else {
                        return $value;
                    }
                    break;
                default:
                    return $value;
                    break;
            }
        }
    }

    /**
     * cleanNumericString
     *
     * @param string $number
     * @param string $thousands
     * @param string $decimals
     * @return string
     */
    public static function cleanNumericString(string $number, string $thousands = null, string $decimals = null)
    {
        $thousands = is_null($thousands) ? self::$thosandsSeparator : $thousands;
        $decimals = is_null($decimals) ? self::$decimalsSeparator : $decimals;
        return str_replace(' ', '', trim(str_replace([$thousands, $decimals], ['', '.'], $number)));
    }

    /**
     * setThousandsSeparator
     *
     * @param string $separator
     * @return void
     */
    public static function setThousandsSeparator(string $separator = '.')
    {
        self::$thosandsSeparator = $separator;
    }

    /**
     * setDecimalsSeparator
     *
     * @param string $separator
     * @return void
     */
    public static function setDecimalsSeparator(string $separator = ',')
    {
        self::$decimalsSeparator = $separator;
    }

    /**
     * setEscapeOpenCharacter
     *
     * @param string $character
     * @return void
     */
    public static function setEscapeOpenCharacter(string $character)
    {
        self::$escapeOpenCharacter = $character;
    }

    /**
     * setEscapeCloseCharacter
     *
     * @param string $character
     * @return void
     */
    public static function setEscapeCloseCharacter(string $character)
    {
        self::$escapeCloseCharacter = $character;
    }
}
