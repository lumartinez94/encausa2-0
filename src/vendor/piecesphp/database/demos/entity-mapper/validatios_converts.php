<?php
use PiecesPHP\Core\Database\EntityMapper;

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once __DIR__ . '/../../vendor/autoload.php';

$array_ints = [
    1,
    1.3,
    '1.3',
    '1,3',
    '1.3.3',
    '123',
    '-123',
    '123.123.123',
    ' 12 3.12 3.12 3  ',
];
$array_reals = [
    1,
    1.3,
    '1.3',
    '-1.3',
    '1,3',
    '1.3.3',
    '123',
    '-123',
    '123.123.123',
    ' 12 3.12 3.12 3  ',
];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test EntityMapper</title>
</head>

<body style="margin:0px;padding:0px 15px;">

    <h2>Validaciones de enteros y conversión a formato válido en SQL</h2>
    <?php foreach ($array_ints as $int): ?>
    <hr>
    <p><strong>Valor:</strong> <?=$int;?></p>
    <p><strong>Válido:</strong> <?=EntityMapper::validateType('int', $int) ? 'Sí' : 'No';?></p>
    <p><strong>Valor convertido:</strong> <?=EntityMapper::castPHPToSQLTypes('int', $int);?></p>
    <?php endforeach;?>

    <h2>Validaciones de float/double y conversión a formato válido en SQL</h2>
    <?php foreach ($array_reals as $real): ?>
    <hr>
    <p><strong>Valor:</strong> <?=$real;?></p>
    <p><strong>Válido:</strong> <?=EntityMapper::validateType('float', $real) ? 'Sí' : 'No';?></p>
    <p><strong>Valor convertido:</strong> <?=EntityMapper::castPHPToSQLTypes('float', $real);?></p>
    <?php endforeach;?>

</body>

</html>