<?php

namespace Mappers;

use PiecesPHP\Core\Database\Exceptions\DatabaseClassesExceptions;
use \Generic\SerializableClass;

error_reporting(E_ALL);
ini_set('display_errors', true);

/* set_error_handler(function ($int_error_type, $string_error_message, $string_error_file, $int_error_line, $array_context) {
if (error_reporting() & $int_error_type) {
throw new \ErrorException($string_error_message, 0, $int_error_type, $string_error_file, $int_error_line);
}
return true;
}); */

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/MainTableMapper.php';
require_once __DIR__ . '/SecondTableMapper.php';
require_once __DIR__ . '/SerializableClass.php';

try {

    $options_connection = [ //Configuración de la conexión
        'driver' => 'mysql',
        'database' => "pcs_databases",
        'user' => "admin",
    ];

    MainTableMapper::setOptions($options_connection); //Establecer las configuraciones
    SecondTableMapper::setOptions($options_connection); //Establecer las configuraciones

    $mapperWithSerializedPropertyNew = new MainTableMapper();
    $serializableObject = new SerializableClass(1);

    $mapperWithSerializedPropertyNew->serialized_column = $serializableObject;

    $mapperWithReference = new SecondTableMapper(); //Mapeador vacío
    $mapperWithReferenceEmpty = new SecondTableMapper(5); //Mapeador con parámetro para llenar los valores
    $mapperWithReferenceNew = new SecondTableMapper(); //Mapeador vacío

    $mapperWithReferenceNew->name = "La'bore v'olupt'ate nesciunt quisquam sapiente amet mol'estiae ipsam nemo sed amet corporis reprehenderit ex cum fugit cumque consequatur magna id";
    $mapperWithReferenceNew->main_table_reference = 1;
    $mapperWithReferenceNew->save();
    $mapperWithReferenceNew = new SecondTableMapper($mapperWithReferenceNew->getLastInsertID()); //Reinstanciado con el id

    header('Content-Type: application/json');

    echo json_encode([
        'MainTableMapper Created' => $mapperWithSerializedPropertyNew->humanReadable(),
        'SecondTableMapper Empty' => $mapperWithReference->humanReadable(),
        'SecondTableMapper Getted' => $mapperWithReferenceEmpty->humanReadable(),
        'SecondTableMapper Created' => $mapperWithReferenceNew->humanReadable(),
    ]);

} catch (DatabaseClassesExceptions $e) {
    header('Content-Type: application/json');

    echo json_encode([
        $e->getMessage(),
        $e->getLine(),
        $e->getCodeString(),
    ]);

    die;
}
