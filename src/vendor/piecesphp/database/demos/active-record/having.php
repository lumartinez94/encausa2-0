<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
use PiecesPHP\Core\Database\Exceptions\DatabaseClassesExceptions;
use \PiecesPHP\Core\Database\ActiveRecordModel;

require_once __DIR__ . '/../../vendor/autoload.php';
$result = [];
try {

    $model = new ActiveRecordModel([
        'driver' => 'mysql',
        'database' => 'pcs_databases',
        'user' => 'admin',
    ]);

    $table_name = 'second_table';
    $joined_table = 'main_table';

    $model->setTable($table_name);

    $model->select([
        "{$table_name}.id AS {$table_name}_id",
        "{$table_name}.name AS {$table_name}_name",
        "{$joined_table}.id AS {$joined_table}_id",
        "{$joined_table}.name AS {$joined_table}_name",
        "{$joined_table}.serialized_column AS {$joined_table}_serialized_column",
    ])->where([
        "{$table_name}.id" => [
            'multiple' => [
                [
                    'and_or' => 'OR',
                    '<' => '2',
                ],
                [
                    'and_or' => 'OR',
                    '=' => '2',
                ],
                [
                    'and_or' => 'OR',
                    '=' => '6',
                ],
            ],
        ],
    ])->innerJoin(
        $joined_table,
        [
            "{$joined_table}.id" => "{$table_name}.main_table_reference",
        ]
    )->where([
        "{$table_name}.id" => [
            '>' => 0,
        ],
    ])->having([
        "{$table_name}_name" => "Werner",
    ]);

    $model->execute();
    $result = $model->result();

} catch (DatabaseClassesExceptions $e) {
    var_dump(
        $e->getMessage(),
        $e->getCodeString()
    );
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test ActiveRecordModel</title>
</head>

<body style="background-color: black; color:#8f9400;">
    <pre>
<strong>
<?=json_encode($result, \JSON_PRETTY_PRINT);?>
</strong>
</pre>
</body>

</html>