<?php
/**
 * LineasMapper.php
 */

namespace App\Model;

use PiecesPHP\Core\BaseEntityMapper;

/**
 * LineasMapper.
 *
 * @author      Juan david nuñez aguilar
 * @copyright   Copyright (c) 2019
 */
class LineasMapper extends BaseEntityMapper
{
    const TABLE = 'lineas_intervencion';

    /**
     * @var string $table
     */
    protected $table = self::TABLE;

    protected $fields = [
        'id' => [
            'type' => 'int',
            'primary_key' => true,
        ],
        'nombre' => [
            'type' => 'varchar',
            'length' => 255,
        ],
        'friendly_url' => [
            'type' => 'varchar',
            'length' => 250,
        ],
        'descripcion' => [
            'type' => 'text',
            'null' => true,
		],
		
		'folder' => [
            'type' => 'text',
            
        ],
        'imagen' => [
            'type' => 'varchar',
            'length' => 255,
        ],
        'icon' => [
            'type' => 'varchar',
            'length' => 100,
        ],
        'estado' => [
            'type' => 'varchar',
            'length' => 100,
        ],
        'date_create' => [
            'type' => 'datetime',
        ],
        'created_by' => [
            'type' => 'int',
        ],
    ];
    /**
     * __construct
     *
     * @param int $value
     * @param string $field_compare
     * @return static
     */
    public function __construct(int $value = null, string $field_compare = 'primary_key')
    {
        parent::__construct($value, $field_compare);
    }

    /**
     * all
     *
     * @param bool $as_mapper
     *
     * @return static[]|array
     */
    public static function all(bool $as_mapper = false)
    {
        $model = self::model();

        $model->select()->execute();

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }

        return $result;
    }

    /**
     * allForSelect
     *
     * @param string $defaultLabel
     * @param string $defaultValue
     * @return array
     */
    public static function allForSelect(string $defaultLabel = 'Lineas de Acción', string $defaultValue = '')
    {
        $options = [];
        $options[$defaultValue] = $defaultLabel;

        array_map(function ($e) use (&$options) {
            $options[$e->id] = $e->nombre;
        }, self::all());

        return $options;
    }

    /**
     * getBy
     *
     * @param mixed $value
     * @param string $column
     * @param boolean $as_mapper
     * @return static|object|null
     */
    public static function getBy($value, string $column = 'id', bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            $column => $value,
        ];

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        $result = count($result) > 0 ? $result[0] : null;

        if (!is_null($result) && $as_mapper) {
            $result = new static($result->id);
        }

        return $result;
    }

    /**
     * existsByID
     *
     * @param int $id
     * @return bool
     */
    public static function existsByID(int $id)
    {
        $model = self::model();

        $where = [
            "id = $id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * existsByName
     *
     * @param string $name
     * @return bool
     */
    public static function existsByName(string $name)
    {
        $model = self::model();

        $where = [
            "name = '$name'",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * existsByFriendlyURL
     *
     * @param string $friendly_url
     * @return bool
     */
    public static function existsByFriendlyURL(string $friendly_url)
    {
        $model = self::model();

        $where = [
            "friendly_url = '$friendly_url'",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * isDuplicate
     *
     * @param string $name
     * @param string $friendly_url
     * @param int $ignore_id
     * @return bool
     */
    public static function isDuplicate(string $name, int $ignore_id)
    {
        $model = self::model();

        $where = [
            "nombre = '$name'",
            "AND id != $ignore_id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * friendlyURLCount
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return bool
     */
    public static function friendlyURLCount(string $friendly_url, int $ignore_id)
    {

        $model = self::model();

        $where = [
            'friendly_url' => $friendly_url,
            'id' => [
                '!=' => $ignore_id,
            ],
        ];

        $model->select('COUNT(id) AS total')->where($where)->execute();

        $result = $model->result();

        return count($result) > 0 ? (int) $result[0]->total : 0;
    }

    /**
     * generateFriendlyURL
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return string
     */
    public static function generateFriendlyURL(string $name, int $ignore_id)
    {
        $friendly_url = friendly_url($name);
        $count_friendly_url = self::friendlyURLCount($friendly_url, $ignore_id);

        if ($count_friendly_url > 0) {
            $friendly_url = $friendly_url . '-' . $count_friendly_url;
        }

        return $friendly_url;
    }

    /**
     * model
     *
     * @return BaseModel
     */
    public static function model()
    {
        return (new static )->getModel();
    }

    public static function SelectWhere()
    {
        $model = self::model();
        $model->select()->where("active = 'activo'")->execute();
        $result = $model->result();

        return $result;
    }

    public static function allData()
    {
        $query = self::model();
        $query->select()
            ->where("estado = 'activo' ORDER BY id ASC");
        $query->execute();
        $resultado = $query->result();
        return $resultado;
    }

    //lineas de intervencion by Id
    public static function datosLineaIntervencion($id)
    {
        $query = self::model();
        $query->select()
            ->where("friendly_url = '$id' ");
        $query->execute();
        $resultado = $query->result();
        return $resultado[0];
    }

    //inner join lineaintervencion - proyectos - usuarios
    public static function innerLineasIntervencionProyectos($id)
    {
        $sql = " SELECT * FROM lineas_intervencion  l
       INNER JOIN proyectos p on  p.id_linea_intervencion = l.id
       INNER JOIN pcsphp_users u on  u.id = p.id_user
       WHERE p.id_linea_intervencion = $id AND p.status = 1 ";
        $stmt = self::model()->prepare($sql);
        $executed = $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_OBJ);

        if ($executed) {
            if (count($result) > 0) {
                return $result;
            } else {
                return false;
            }
        } else {
            return -1;
        }
    }
}
