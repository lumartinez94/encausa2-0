<?php
/**
 * ProyectosMapper.php
 */

namespace App\Model;

use PiecesPHP\Core\BaseEntityMapper;

/**
 * ProyectosMapper.
 *
 * @author      Juan david nuñez aguilar
 * @copyright   Copyright (c) 2019
 */
class ProyectosMapper extends BaseEntityMapper
{
    const TABLE = 'proyectos';

    /**
     * @var string $table
     */
    protected $table = self::TABLE;

    protected $fields = [
        'id' => [
            'type' => 'int',
            'primary_key' => true,
        ],
        'titulo' => [
            'type' => 'varchar',
            'length' => 255,
        ],
        'subtitulo' => [
            'type' => 'varchar',
            'length' => 255,
        ],
        'id_linea_intervencion' => [
            'type' => 'int',
        ],
        'status' => [
            'type' => 'int',
        ],
        'descripcion' => [
            'type' => 'text',
        ],
        'seo_url' => [
            'type' => 'varchar',
            'length' => 100,
        ],
        'folder' => [
            'type' => 'varchar',
            'length' => 225,
		],
		
		'images'=>[
			'type'=>'json',
			'null'=>true,
		],

        'fecha_reg' => [
            'type' => 'datetime',
        ],
        'id_user' => [
            'type' => 'int',
        ],
    ];
    /**
     * __construct
     *
     * @param int $value
     * @param string $field_compare
     * @return static
     */
    public function __construct(int $value = null, string $field_compare = 'primary_key')
    {
        parent::__construct($value, $field_compare);
    }

    /**
     * all
     *
     * @param bool $as_mapper
     *
     * @return static[]|array
     */
    public static function all(bool $as_mapper = false)
    {
        $model = self::model();

        $model->select()->execute();

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id);
            }, $result);
        }

        return $result;
    }

    /**
     * allForSelect
     *
     * @param string $defaultLabel
     * @param string $defaultValue
     * @return array
     */
    public static function allForSelect(string $defaultLabel = 'Proyectos', string $defaultValue = '')
    {
        $options = [];
        $options[$defaultValue] = $defaultLabel;

        array_map(function ($e) use (&$options) {
            $options[$e->id] = $e->titulo;
        }, self::all());

        return $options;
    }

    /**
     * innerProyectos-usuarios
     *
     * En caso de existir devuelve el objeto del resultado, si no existe
     * devuele false y si hubo un error devuelve -1
     *
     * @param string $seo_url
     * @return \stdClass|bool|int
     */
    public static function innerProyectos($seo_url)
    {
        $table = 'proyectos';
        $inner_table = 'pcsphp_users';
        $table_fields = [
            "$table.id",
            "$table.titulo",
            "$table.id_linea_intervencion",
            "$table.status",
            "$table.descripcion",
            "$table.fecha_reg",
			"$table.folder",
			"$table.images",
            "$table.seo_url",
            "$table.id_user",
            "$inner_table.username",
            "$inner_table.firstname",
            "$inner_table.first_lastname",
            //"$inner_table.avatar",
            //"$inner_table.linkedin",
        ];
        $where = [
            "$table.seo_url = :SEO_URL",
        ];
        $on = [
            "$inner_table.id = $table.id_user",
        ];

        $table_fields = implode(', ', $table_fields);
        $where = implode(', ', $where);
        $on = implode(', ', $on);

        $inner_sql = "INNER JOIN $inner_table ON $on";

        $sql = "SELECT $table_fields FROM $table %INNER% WHERE $where";

        $sql = str_replace('%INNER%', $inner_sql, $sql);
        $sql = trim($sql);

        $stmt = self::model()->prepare($sql);
        $executed = $stmt->execute([
            ':SEO_URL' => $seo_url,
        ]);
        $result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        if ($executed) {
            if (count($result) > 0) {
                return $result[0];
            } else {
                return false;
            }
        } else {
            return -1;
        }
    }

    /**
     * Relacion entre lineas de intervencion y proyectos
     **/
    public static function innerProyectosLineas()
    {
        $model = self::model();
        $model->select("proyectos.id as 'idProyectos',
        proyectos.titulo,
        proyectos.subtitulo,
        proyectos.id_linea_intervencion,
        proyectos.status,
        proyectos.descripcion,
		proyectos.folder,
		proyectos.images,
        proyectos.seo_url,
        lineas_intervencion.id as 'idLineaIntervencion',
        lineas_intervencion.nombre,
        lineas_intervencion.descripcion,
        lineas_intervencion.imagen")->join("lineas_intervencion", "lineas_intervencion.id =proyectos.id_linea_intervencion")->where("proyectos.status = 1");

        $model->execute();

        $result = $model->result();
        $result = count($result) > 0 ? $result : -1;

        return $result;
    }

    /**
     * getBy
     *
     * @param mixed $value
     * @param string $column
     * @param boolean $as_mapper
     * @return static|object|null
     */
    public static function getBy($value, string $column = 'id', bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            $column => $value,
        ];

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        $result = count($result) > 0 ? $result[0] : null;

        if (!is_null($result) && $as_mapper) {
            $result = new static($result->id);
        }

        return $result;
    }

    /**
     * existsByID
     *
     * @param int $id
     * @return bool
     */
    public static function existsByID(int $id)
    {
        $model = self::model();

        $where = [
            "id = $id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * existsByName
     *
     * @param string $name
     * @return bool
     */
    public static function existsByName(string $name)
    {
        $model = self::model();

        $where = [
            "name = '$name'",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * existsByFriendlyURL
     *
     * @param string $friendly_url
     * @return bool
     */
    public static function existsByFriendlyURL(string $friendly_url)
    {
        $model = self::model();

        $where = [
            "seo_url = '$friendly_url'",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * isDuplicate
     *
     * @param string $name
     * @param string $friendly_url
     * @param int $ignore_id
     * @return bool
     */
    public static function isDuplicate(string $name, string $friendly_url, int $ignore_id)
    {
        $model = self::model();

        $where = [
            "(titulo = '$name'",
            "OR seo_url = '$friendly_url')",
            "AND id != $ignore_id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * friendlyURLCount
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return bool
     */
    public static function friendlyURLCount(string $friendly_url, int $ignore_id)
    {

        $model = self::model();

        $where = [
            'seo_url' => $friendly_url,
            'id' => [
                '!=' => $ignore_id,
            ],
        ];

        $model->select('COUNT(id) AS total')->where($where)->execute();

        $result = $model->result();

        return count($result) > 0 ? (int) $result[0]->total : 0;
    }

    /**
     * generateFriendlyURL
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return string
     */
    public static function generateFriendlyURL(string $name, int $ignore_id)
    {
        $friendly_url = friendly_url($name);
        $count_friendly_url = self::friendlyURLCount($friendly_url, $ignore_id);

        if ($count_friendly_url > 0) {
            $friendly_url = $friendly_url . '-' . $count_friendly_url;
        }

        return $friendly_url;
    }

    /**
     * model
     *
     * @return BaseModel
     */
    public static function model()
    {
        return (new static )->getModel();
    }

    public static function SelectWhere()
    {
        $model = self::model();
        $model->select()->where("active = 'activo'")->execute();
        $result = $model->result();

        return $result;
    }

    public static function allData()
    {
        $query = self::model();
        $query->select()
            ->where("estado = 'activo' ORDER BY id ASC");
        $query->execute();
        $resultado = $query->result();
        return $resultado;
    }

    //----------------------------------
    //proyecto anterior
    public static function lastProject($id)
    {
        $id = (int) $id;
        $model = self::model();
        $model->select()->where("id < $id AND status = 1")->orderBy("id DESC")->execute();
        $result = $model->result();

        return count($result) > 0 ? $result[0] : -1;

    }
    //proyecto Siguiente
    public static function nextProject($id)
    {
        $id = (int) $id;

        $model = self::model();

        $model->select()->where("id > $id AND status = 1")->orderBy("id ASC")->execute();
        $result = $model->result();

        return count($result) > 0 ? $result[0] : -1;

    }

    //inner join noticias - proyectos - usuarios
    public static function listaNoticiasPorProyecto($seo_url)
    {
        $sql = " SELECT *,news.seo_url as 'seourlNew',news.images as imageNews FROM news
        INNER JOIN proyectos on proyectos.id = news.id_proyecto
        WHERE proyectos.seo_url = '$seo_url' AND news.status_new = 'approved' ";

        $stmt = self::model()->prepare($sql);
        $executed = $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_OBJ);

        if ($executed) {
            if (count($result) > 0) {
                return $result;
            } else {
                return false;
            }
        } else {
            return -1;
        }
    }
}
