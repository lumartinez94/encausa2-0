<?php
/**
 * NewsMapper.php
 */

namespace App\Model;

use PiecesPHP\Core\BaseEntityMapper;

/**
 * NewsMapper.
 *
 * @package     PiecesPHP\BuiltIn\Article\Category\Mappers
 * @author      Juan Nuñez
 * @copyright   Copyright (c) 2019
 */
class NewsMapper extends BaseEntityMapper
{
    const TABLE = 'news';

    /**
     * @var string $table
     */
    protected $table = self::TABLE;

    protected $fields = [
        'id_new' => [
            'type' => 'int',
            'primary_key' => true,
        ],
        'id_proyecto' => [
            'type' => 'int',
        ],
        'title_new' => [
            'type' => 'varchar',
            'length' => 250,
        ],
        'seo_url' => [
            'type' => 'varchar',
            'length' => 250,
        ],
        'sub_title_new' => [
            'type' => 'varchar',
            'length' => 150,
        ],
        'body_new' => [
            'type' => 'text',
            'null' => true,
        ],
        'status_new' => [
            'type' => 'varchar',
            'length' => 250,
        ],
        'created_by' => [
            'type' => 'int',
        ],
        'approved_by' => [
            'type' => 'int',
        ],
        'folder_name' => [
            'type' => 'varchar',
            'length' => 250,
		],

		'images' => [
            'type' => 'json',
            'null' => true,
		],
		
        'visits' => [
            'type' => 'int',
        ],
        'cat_id' => [
            'type' => 'int',
        ],
        'created_at' => [
            'type' => 'datetime',
        ],
    ];
    /**
     * __construct
     *
     * @param int $value
     * @param string $field_compare
     * @return static
     */
    public function __construct(int $value = null, string $field_compare = 'primary_key')
    {
        parent::__construct($value, $field_compare);
    }

    /**
     * all
     *
     * @param bool $as_mapper
     *
     * @return static[]|array
     */
    public static function all(bool $as_mapper = false)
    {
        $model = self::model();

        $model->select()->execute();

        $result = $model->result();

        if ($as_mapper) {
            $result = array_map(function ($e) {
                return new static($e->id_new);
            }, $result);
        }

        return $result;
    }

    /**
     * allForSelect
     *
     * @param string $defaultLabel
     * @param string $defaultValue
     * @return array
     */
    public static function allForSelect(string $defaultLabel = 'Categorías', string $defaultValue = '')
    {
        $options = [];
        $options[$defaultValue] = $defaultLabel;

        array_map(function ($e) use (&$options) {
            $options[$e->id_new] = $e->name;
        }, self::all());

        return $options;
    }

    /**
     * getBy
     *
     * @param mixed $value
     * @param string $column
     * @param boolean $as_mapper
     * @return static|object|null
     */
    public static function getBy($value, string $column = 'id_new', bool $as_mapper = false)
    {
        $model = self::model();

        $where = [
            $column => $value,
        ];

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        $result = count($result) > 0 ? $result[0] : null;

        if (!is_null($result) && $as_mapper) {
            $result = new static($result->id_new);
        }

        return $result;
    }

    /**
     * existsByID
     *
     * @param int $id_new
     * @return bool
     */
    public static function existsByID(int $id_new)
    {
        $model = self::model();

        $where = [
            "id_new = $id_new",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * existsByName
     *
     * @param string $name
     * @return bool
     */
    public static function existsByName(string $name)
    {
        $model = self::model();

        $where = [
            "name = '$name'",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * existsByFriendlyURL
     *
     * @param string $friendly_url
     * @return bool
     */
    public static function existsByFriendlyURL(string $friendly_url)
    {
        $model = self::model();

        $where = [
            "seo_url = '$friendly_url'",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * isDuplicate
     *
     * @param string $name
     * @param string $friendly_url
     * @param int $ignore_id
     * @return bool
     */
    public static function isDuplicate(string $name, string $friendly_url, int $ignore_id)
    {
        $model = self::model();

        $where = [
            "(title_new = '$name'",
            "OR seo_url = '$friendly_url')",
            "AND id_new != $ignore_id",
        ];
        $where = trim(implode(' ', $where));

        $model->select()->where($where);

        $model->execute();

        $result = $model->result();

        return count($result) > 0;
    }

    /**
     * friendlyURLCount
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return bool
     */
    public static function friendlyURLCount(string $friendly_url, int $ignore_id)
    {

        $model = self::model();

        $where = [
            'seo_url' => $friendly_url,
            'id_new' => [
                '!=' => $ignore_id,
            ],
        ];

        $model->select('COUNT(id_new) AS total')->where($where)->execute();

        $result = $model->result();

        return count($result) > 0 ? (int) $result[0]->total : 0;
    }

    /**
     * generateFriendlyURL
     *
     * @param string $friendly_url
     * @param int $ignore_id
     * @return string
     */
    public static function generateFriendlyURL(string $name, int $ignore_id)
    {
        $friendly_url = friendly_url($name);
        $count_friendly_url = self::friendlyURLCount($friendly_url, $ignore_id);

        if ($count_friendly_url > 0) {
            $friendly_url = $friendly_url . '-' . $count_friendly_url;
        }

        return $friendly_url;
    }

    /**
     * model
     *
     * @return BaseModel
     */
    public static function model()
    {
        return (new static )->getModel();
    }

    //Listado de noticias
    public function listadoNoticias()
    {
        return $this->getAll();
    }

    //Noticias Activas
    public static function noticiasActivas()
    {
        $query = self::model();
        $query->select()
            ->where("status_new = 'approved'")->orderBy('created_at desc');
        $query->execute();
        $resultado = $query->result();
        return $resultado;
    }

    //Noticias Activas limite
    public static function noticiasActivasIndex($limit = 9)
    {
        $query = self::model();
        $query->select()
            ->where("status_new = 'approved' order by id_new desc limit $limit");
        $query->execute();
        $resultado = $query->result();
        return $resultado;
    }

    //carga Noticias - Noticia anterior
    public static function noticiaAnterior($contador)
    {
        $model = self::model();

        $model->select()->where('status_new = "approved"')->orderBy('id_new DESC')->execute(false, $contador, 6);

        $result = $model->result();

        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }

    }

    /**
     * cantidadNoticias
     * @return bool
     */
    public static function cantidadNoticias()
    {

        $model = self::model();

        $where = [
            'status_new' => "approved",
        ];

        $model->select('COUNT(id_new) AS count')->where($where)->execute();

        $result = $model->result();

        return count($result) > 0 ? (int) $result[0]->count : -1;
    }

    /**
     * innerProyectos
     **/
    public static function innerNoticias($seo_url)
    {
        $model = self::model();

        $model->select()->join("pcsphp_users")->where("status_new = 'approved' AND seo_url = '$seo_url'")->execute();

        $result = $model->result();

        return count($result) > 0 ? $result[0] : -1;

    }

    //----------------------------------
    //noticia anterior
    public static function lastNew($id_new)
    {
        $model = self::model();
        $model->select()->where('id_new < ' . $id_new . ' and status_new = "approved" order by id_new desc limit 1')->execute();
        $result = $model->result();

        return count($result) > 0 ? $result[0] : -1;

    }
    //Noticia Siguiente
    public static function nextNew($id_new)
    {
        $model = self::model();

        $model->select()->where('id_new > ' . $id_new . ' and status_new = "approved" order by id_new asc limit 1')->execute();
        $result = $model->result();

        return count($result) > 0 ? $result[0] : -1;

    }

    public static function contadorVisitas($id_new, $cantidad)
    {
        $cantidad = $cantidad + 1;
        $model = self::model();
        $model->update([
            'visits' => $cantidad,
        ])->where('id_new = ' . $id_new);
        $model->execute();
        return $model;
    }
}
