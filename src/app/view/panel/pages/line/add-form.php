<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<div style="max-width:850px;">

    <h3><?=__('lineBackend','Agregar')?> <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>" >
        <div class="ui top attached tabular menu">
            <div class="active item" data-tab="item-1"><?= __('lineBackend','Datos Lineas de intervención')?></div>
            <div class="item" data-tab='item-2'><?= __('lineBackend','Portada de lineas de intervecíon')?></div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="item-2">
            <div class="ui form cropper-adapter" cropper-line>

                <div class="field required">
                    <label><?= __('lineBackend', 'Portada'); ?></label>
                    <input type="file" accept="image/*" name="portada" required>
                </div>

                <?php $this->_render('panel/built-in/utilities/cropper/workspace.php', 
					[
						'referenceW'=> '800',
						'referenceH'=> '600',
					]); 
				?>

            </div>
            <div class="field">
                <button type="submit" class="ui button green"><?= __('lineBackend', 'Guardar'); ?></button>
            </div>
        </div>
        <div class="ui bottom attached tab segment active" data-tab="item-1">

            <div class="field">
                <div class="ui slider checkbox">
                    <input type="checkbox" name="status">
                    <label><?= __('lineBackend','Estado') ?></label>
                </div>
            </div>
            <br>


            <div class="field required">
                <label><?= __('lineBackend', 'Nombre'); ?></label>
                <input required type="text" name="name" maxlength="105">
            </div>

            <div class="field required">
                <label><?= __('lineBackend', 'Descripción'); ?></label>
                <div quill-editor></div>
                <textarea name="description" required></textarea>
            </div>

            <div class="field ">
                <label><?= __('lineBackend', 'Icono'); ?></label>
                <input type="file" name="icon">
            </div>
        </div>




    </form>
</div>
