<?php
defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");
/**
 * @var PiecesPHP\BuiltIn\Article\Mappers\ArticleMapper $element
 */
$element;
?>

<div style="max-width:850px;">

    <h3><?= __('lineBackend','Editar')?> <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>">
        <div class="ui top attached tabular menu">
            <div class="active item" data-tab="item-1"><?= __('lineBackend','Datos Lineas de intervención')?></div>
            <div class="item" data-tab='item-2'><?= __('lineBackend','Portada de lineas de intervecíon')?></div>
        </div>
        <input type="hidden" name="id" value="<?=$element->id;?>">

        <div class="ui bottom attached tab segment" data-tab="item-2">


            <div class="ui form cropper-adapter" cropper-line>

                <div class="field">
                    <label><?= __('articlesBackend', 'Portada'); ?></label>
                    <input type="file" name="portada" accept="image/*">
                </div>

                <?php $this->_render('panel/built-in/utilities/cropper/workspace.php', [
											'referenceW'=> '800',
											'referenceH'=> '600',
											'image' =>$element->imagen,
											]); ?>

            </div>

            <div class="field">
                <button type="submit" class="ui button green"><?= __('lineBackend','Guardar')?></button>
            </div>
        </div>
        <div class="ui bottom attached tab segment active" data-tab="item-1">

            <div class="field">
                <div class="ui slider checkbox status-checkbox">
                    <input type="checkbox" name="status" class="status-value" value="<?=$element->estado == "activo" ? true : false?>">
                    <label><?= __('lineBackend', 'Estado'); ?></label>
                </div>
            </div>
            <div class="field required">
                <label><?= __('lineBackend', 'Nombre'); ?></label>
                <input required type="text" name="name" maxlength="105" value="<?=$element->nombre;?>">
            </div>

            <div class="field required">
                <label><?= __('lineBackend', 'Descripción'); ?></label>
                <div quill-editor><?=$element->descripcion; ?></div>
                <textarea name="description" required><?=$element->descripcion; ?></textarea>
            </div>
            <div class="field ">
                <label><?= __('lineBackend','Icono')?></label>
                <input type="file"  name="icon">
            </div>
        </div>


    </form>
</div>
