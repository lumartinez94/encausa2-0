<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<div style="max-width:850px;">

    <h3><?=__('projectBackend','Agregar')?> <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form"  quill="<?=$quill_proccesor_link;?>" >
        <div class="ui top attached tabular menu">
            <div class="active item" data-tab="item-1"><?= __('projectBackend','datos del proyecto') ?></div>
            <div class="item" data-tab="item-2"><?=  __('projectBackend','Portada del proyecto') ?></div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="item-2">
            <div class="ui form cropper-adapter" cropper-project>

                <div class="field required">
                    <label><?= __('projectBackend', 'Portada'); ?></label>
                    <input type="file" accept="image/*" name="portada" required>
                </div>

				<?php $this->_render('panel/built-in/utilities/cropper/workspace.php', 
						[
							'referenceW'=> '800',
							'referenceH'=> '600',
						]); 
				?>

            </div>
            <div class="field">
                <button type="submit" class="ui button green"><?=__('projectBackend','Guardar')?></button>
            </div>
        </div>
        <div class="ui bottom attached tab segment active" data-tab="item-1">
            <div class="field required">
                <label><?=__('projectBackend','Título')?></label>
                <input required type="text" name="title" maxlength="255">
            </div>
            <div class="field required">
                <label><?=__('projectBackend','Subtitulo')?></label>
                <input required type="text" name="subtitle" maxlength="255">
            </div>
            <div class="field required">
                <label><?=__('projectBackend','Lineas de Acción')?></label>
                <select required class='ui dropdown' name="line"><?=$options_lineas;?></select>
			</div>
			
			
			<div class="field required">
                <label><?= __('articlesBackend', 'Descripción'); ?></label>
                <div quill-editor></div>
                <textarea name="description" required></textarea>
            </div>

            
            <div class="field">
                <div class="ui slider checkbox">
                    <input type="checkbox" name="status">
                    <label><?=__('projectBackend','Estado')?></label>
                </div>	

            </div>
        </div>



    </form>
</div>


