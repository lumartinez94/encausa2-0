<?php
defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");
/**
 * @var PiecesPHP\BuiltIn\Article\Mappers\ArticleMapper $element
 */
$element;
?>

<div style="max-width:850px;">

    <h3><?= __('projectBackend','Editar')?> <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>">

		<input type="hidden" name="id" value="<?=$element->id;?>">
		<div class="ui top attached tabular menu">
			<div class="active item" data-tab="item-1"><?= __('projectBackend','Datos del proyecto')?></div>
			<div class="item" data-tab="item-2"><?= __('projectBackend','Portada del proyecto')?></div>
		</div>	
		<div class="ui bottom attached tab segment" data-tab="item-2">	
    
		
            <div class="ui form cropper-adapter" cropper-project>

                <div class="field">
                    <label><?= __('articlesBackend', 'Portada'); ?></label>
                    <input type="file" name="portada" accept="image/*">
                </div>

                <?php $this->_render('panel/built-in/utilities/cropper/workspace.php', [
											'referenceW'=> '800',
											'referenceH'=> '600',
											'image' =>$element->images,
											]); ?>

            </div>
        
		<div class="field">
            <button type="submit" class="ui button green"><?= __('projectBackend','Guardar')?></button>
		</div>
		
		
		</div>
		<div class="ui bottom attached tab segment active" data-tab="item-1">
        <div class="field required">
            <label>Título</label>
            <input required type="text" name="title" maxlength="255" value="<?=$element->titulo;?>">
        </div>
        <div class="field required">
            <label>Subtitulo</label>
            <input required type="text" name="subtitle" value="<?=$element->subtitulo;?>" maxlength="255">
        </div>
        <div class="field required">
            <label>Lineas de Acción</label>
            <select required class='ui dropdown' name="line"><?=$options_lineas;?></select>
        </div>

		<div class="field required">
                <label><?= __('articlesBackend', 'Descripción'); ?></label>
                <div quill-editor><?=$element->descripcion; ?></div>
                <textarea name="description" required><?=$element->descripcion; ?></textarea>
            </div>
        <div class="field">
            <div class="ui slider checkbox status-project-value">
                <input type="checkbox" name="status" class="status-value" value="<?=$element->status?>">

                <label>Estado</label>
            </div>

        </div>
       

    </form>
</div>

<script>
window.onload = () => {
	$('.ui.dropdown').dropdown();
	$('.tabular.menu .item').tab();

}
</script>
