<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<div style="max-width:850px;">

    <h3><?=__('newsBackend','Agregar')?> <?=$title;?></h3>

    <div class="ui buttons">
        <a href="<?=$back_link;?>" class="ui button blue"><i class="icon left arrow"></i></a>
    </div>

    <br><br>

    <form action-form method='POST' action="<?=$action;?>" class="ui form" quill="<?=$quill_proccesor_link;?>">
        <div class="ui top attached tabular menu">
            <div class="active item" data-tab="item-1"><?= __('newsBackend','Datos de Noticias')?></div>
            <div class="item" data-tab='item-2'><?= __('newsBackend','Imagenes de Noticias')?></div>
        </div>
        <div class="ui bottom attached tab segment" data-tab="item-2">
            <div class="ui form cropper-adapter" cropper-news>

                <div class="field required">
                    <label><?= __('lineBackend', 'Fondo'); ?></label>
                    <input type="file" accept="image/*" name="portada" required>
                </div>

                <?php $this->_render('panel/built-in/utilities/cropper/workspace.php', 
					[
						'referenceW'=> '800',
						'referenceH'=> '600',
					]); 
				?>

            </div>
            <div class="ui form cropper-adapter" cropper-newsm>

                <div class="field required">
                    <label><?= __('lineBackend', 'Portada'); ?></label>
                    <input type="file" accept="image/*" name="portadasm" required>
                </div>

                <?php $this->_render('panel/built-in/utilities/cropper/workspace.php', 
					[
						'referenceW'=> '600',
						'referenceH'=> '600',
					]); 
				?>

            </div>
            <div class="field">
                <button type="submit" class="ui button green"><?= __("newsBackend","Guardar") ?></button>
            </div>

        </div>
        <div class="ui bottom attached tab segment active" data-tab="item-1">
            <div class="field required">
                <label><?= __("newsBackend","Título") ?></label>
                <input required type="text" name="title" maxlength="255">
            </div>
            <div class="field required">
                <label><?= __("newsBackend","Subtitulo") ?></label>
                <input required type="text" name="subtitle" maxlength="150">
            </div>
            <div class="field required">
                <label><?= __("newsBackend","Proyecto asociado") ?></label>
                <select required class='ui dropdown' name="project"><?=$options_projects;?></select>
            </div>


            <div class="field required">
                <label><?= __('newsBackend', 'Descripción'); ?></label>
                <div quill-editor></div>
                <textarea name="description" required></textarea>
            </div>

            <div class="field">
                <div class="ui slider checkbox">
                    <input type="checkbox" name="status">
                    <label><?= __("newsBackend","Estado") ?></label>
                </div>

            </div>


        </div>

    </form>
</div>
