<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<!DOCTYPE html>
<html lang="<?=get_config('app_lang');?>">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <base href="<?=baseurl();?>">
    <?=\PiecesPHP\Core\Utilities\Helpers\MetaTags::getMetaTagsGeneric();?>
    <?=\PiecesPHP\Core\Utilities\Helpers\MetaTags::getMetaTagsOpenGraph();?>
    <link rel="shortcut icon" href="<?=get_config('favicon');?>" type="image/x-icon">
    <?php load_css(['base_url' => "", 'custom_url' => ""])?>
</head>

<body>
    <div class="global-layout">

        <header class="encausa header sobreposicion"
            >
            <div class="content">
                <h1><?=isset($titulo) ? $titulo : '';?></h1>
                <p><?=isset($descripcion) ? $descripcion : '';?></p>
            </div>
        </header>
	