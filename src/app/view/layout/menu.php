<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<nav class="navbar">
    <div class="logo">
        <a href="<?=baseurl()?>">
            <img src="<?=baseurl()?>statics/images/logo.svg">
        </a>
    </div>
    <ul class="enlaces">
        <li <?php echo linkActive($view, 'home'); ?>>
            <a href='<?=baseurl()?>'>Inicio</a>
        </li>
        <li class='sub-item <?php echo linkActive($view, 'balance', true);
echo linkActive($view, 'about_us', true); ?>'>
            <a>La fundación</a>
            <ul>
                <li>
                    <a href='<?=get_route('public-balance')?>'>Balance social</a>
                </li>

                <li>
                    <a href='<?=get_route('public-about-us')?>'>Quiénes somos</a>
                </li>
            </ul>
        </li>
        <li <?php echo linkActive($view, 'lineas-intervencion'); ?>>
            <a href='<?=baseurl('lineas-intervencion')?>'>Lineas de intervención</a>
        </li>
        <li <?php echo linkActive($view, 'proyectos'); ?>>
            <a href='<?=get_route('public-proyectos')?>'>Proyectos</a>
        </li>
        <li class="mail" style="cursor: pointer">
            <a class="formularioContacto"> <span class="icon email"><img src="<?=baseurl()?>statics/images/webmail.svg"
                        alt=""></span>
            </a>
        </li>
        <button class="btn-menu">
            <span class="bar"></span>
            <span class="text">Menú</span>
        </button>
    </ul>

    <div class="formContacto" style="display:none; ">
        <div class="contenedor">
            <h2>CONTÁCTENOS</h2>
            <ul>
                <li>
                    <a target="_blank" href="https://www.google.com/maps/@11.0059669,-74.8097702,15z">Torices Calle 48 #
                        13-114 Edificio Aqualina Oficina 1107</a>
                </li>
                <li>
                    <a href="tel:57-5 6642980">
                        +57 5 664 2980 &nbsp;&nbsp;&nbsp;
                    </a>

                    <a href="tel:+57 313 893 7772">
                        +57 313 893 7772
                    </a>
                </li>

                <li><a class="correo" href="mailto:contacto@encausa.org">contacto@encausa.org</a> </li>
                <li class="social-media">
                    <a href="">
                        <img src="<?=base_url('statics/images/icons/facebook.svg')?>" alt="facebook" width="30px">
                    </a>
                    <a href="">
                        <img src="<?=base_url('statics/images/icons/instagram.svg')?>" alt="Instragam" width="30px">
                    </a>
                    <a href="">
                        <img src="<?=base_url('statics/images/icons/youtube.svg')?>" alt="Youtube" width="30px">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<div class="overlay menu-overlay">
    <nav class="small-nav">
        <ul style="list-style:none;">
            <div>
                <li>
                    <a href='<?=baseurl()?>'>Inicio</a>
                </li>
                <li class='sub-item'>
                    <a>La fundación</a>
                    <ul>
                        <li>
                            <a href='<?=get_route('public-balance')?>'>Balance social</a>
                        </li>

                        <li>
                            <a href='<?=get_route('public-about-us')?>'>Quiénes somos</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href='<?=baseurl('lineas-intervencion')?>'>Lineas de intervención</a>
                </li>
                <li>
                    <a href='<?=baseurl('proyectos')?>'>Proyectos</a>
                </li>

            </div>

            <div class="info-container">

                <li class="information">
                    <a class="direction" target="_blank" href="https://www.google.com/maps/@11.0059669,-74.8097702,15z">Torices Calle 48 #
                        13-114 <br> Edificio Aqualina Oficina 1107</a>
                    <a href="tel:57-5 6642980">
                        +57 5 664 2980 &nbsp;&nbsp;&nbsp;
                    </a>

                    <a href="tel:+57 313 893 7772">
                        +57 313 893 7772
                    </a>
                    <a class="correo" href="mailto:contacto@encausa.org">contacto@encausa.org</a>
                </li>



                <li class="social-media">
                    <a href="">
                        <img src="<?=base_url('statics/images/icons/orange_facebook.svg')?>" alt="facebook"
                            width="40px">
                    </a>
                    <a href="">
                        <img src="<?=base_url('statics/images/icons/orange_instagram.svg')?>" alt="Instragam"
                            width="40px">
                    </a>
                    <a href="">
                        <img src="<?=base_url('statics/images/icons/orange_youtube.svg')?>" alt="Youtube" width="40px">
                    </a>
                </li>
            </div>
        </ul>
    </nav>
</div>
