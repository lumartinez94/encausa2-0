<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<footer>
    <div class="logo">
        <img src="<?=baseurl()?>statics/images/logo.svg">
        <div class="contacto">
            <address>Torices Calle 48 # 13 -114 Edificio Aqualina Oficina 1107</address>
            <address>+57 5 6642980 - +57 313 893 7772</address>
            <address></address>
            <address>
                <strong>
                    <a href="mailto:contacto@encausa.org">contacto@encausa.org</a>
                </strong>
            </address>
        </div>
    </div>
    <div class="enlaces">
        <h3>Siguenos en:</h3>
        <div>
            <a target="blank" href="#">
                <img src="<?=base_url('statics/images/icons/orange_facebook.svg')?>" alt="facebook">
            </a>
            <a target="blank" href="#">
                <img src="<?=base_url('statics/images/icons/orange_instagram.svg')?>" alt="instagram">
            </a>
            <a target="blank" href="#">
                <img src="<?=base_url('statics/images/icons/orange_youtube.svg')?>" alt="youtube">
            </a>
        </div>
    </div>
</footer>
</div>
<?php load_js(['base_url' => "", 'custom_url' => ""])?>
</body>

</html>
