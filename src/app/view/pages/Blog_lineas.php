<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<section class="loader">
    <ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</section>


<section class="carousel carouselLineasdeintervencion">
    <img src="<?=baseurl($datosLineaIntervencion->imagen)?>" alt="cover">
    <div class="carousel-cont">
        <div>
            <h2 id="title"> <?=$datosLineaIntervencion->nombre?> </h2>
        </div>
    </div>
    <div class="blur"></div>
</section>

<section class="text">
    <div class="body-text" id="body_text">
        <?=$datosLineaIntervencion->descripcion?>
    </div>
    <div>

        <section class="cuadros categorias">
            <?php
if ($Proyectos_LineaIntervencion) {
    foreach ($Proyectos_LineaIntervencion as $linea_proyectos) {
        ?>
            <a href="<?=get_route('public-blog-proyecto', ["url-friendly" => $linea_proyectos->seo_url])?>"
                class="categoria"
                style="background-image:url(<?=baseurl("$linea_proyectos->images")?>">
                <div class="content">
                    <h3><?=$linea_proyectos->titulo?></h3>
                    <h4><?=$linea_proyectos->username?></h4>
                </div>
                <div class="courtain"></div>

            </a>
            <?php
}
}
?>
        </section>

    </div>
</section>


