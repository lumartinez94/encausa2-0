<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<section class="loader">
    <ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</section>
<?php
$fecha = explode(' ', $datosNoticias->created_at);
$fecha = $fecha[0];
$fecha = explode("-", $fecha);
$fecha = "$fecha[2]-$fecha[1]-$fecha[0] ";
$NoticiasImagenees=json_decode($datosNoticias->images);
?>
<section class="carousel carouselNoticias">
    <img src="<?=baseurl("$NoticiasImagenees->imageFondo")?> " alt="cover">

    <div class="carousel-cont">
        <div>
            <h2 id="title"> <?=$datosNoticias->title_new?> </h2>
            <span> <span id="type"> </span> Blog | <span id="date"> <?=$fecha?> </span> </span>
        </div>
    </div>

    <div class="blur"></div>
</section>

<section class="text">
    <div class="body-text" id="body_text">
        <?=$datosNoticias->body_new?>
    </div>
</section>




<section class="more-news ">
    <div id="items" class="<?php if ($lastNew == '' || $nextNew == '') {echo 'one';}?>">
        <?php

if ($lastNew != '') {
    ?>
        <div>
            <div class="inverted">
                <div>
                    <h3>
                        <a class="title" href="blog/<?=$lastNew->seo_url?>">
                            Anterior
                            <div>
                                <h1><?=$lastNew->title_new?></h1>
                            </div>
                        </a>
                    </h3>
                    <label class="date" for="">Blog | <?=$lastNew->created_at?></label>
                </div>
            </div>
        </div>
        <?php
}
if ($nextNew != '') {
    ?>
        <div>
            <div class="inverted">
                <div class="left">
                    <h3>
                        <a class="title" href="blog/<?=$nextNew->seo_url?>">
                            Siguiente
                            <div>
                                <h1><?=$nextNew->title_new?></h1>
                            </div>
                        </a>
                    </h3>
                    <label class="date" for="">Blog | <?=$nextNew->created_at?></label>
                </div>
            </div>
        </div>
        <?php
}
?>
    </div>
</section>
