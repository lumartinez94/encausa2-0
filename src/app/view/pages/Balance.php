<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<section class="encausa balance categorias">
    <article class="categoria alt square-1">
        <div class="content">
            <span class="number">425</span>
            <h3 class="orange">Jornadas de salud, prevencion y pronacion a niños, jovenes y adultos.</h3>
        </div>
    </article>
    <article class="categoria square-2">
        <div class="content-2">
            <span class="number">3.560</span>
            <h3 class="white">Creacion de negocios de poblacion vulnerable en la costa carbe y eje cafetero.</h3>
        </div>
    </article>
    <article class="categoria alt square-3">
        <div class="content">
            <span class="number">25</span>
            <h3 class="white">Unidades proactivas en todo el pais.</h3>
        </div>
    </article>
    <article class="categoria square-4">
        <div class="content-2">
            <span class="number">872</span>
            <h3 class="white">Plan padrino para rehabilitacion de niños, jovenes y personas con discapacidad.</h3>
        </div>
    </article>
    <article class="categoria alt square-5">
        <div class="content">
            <span class="number">1672</span>
            <h3 class="orange">Proyectos lúdicos y recreativos con niños y niñas de la costa caribe.</h3>
        </div>
    </article>
    <article class="categoria square-6">
        <div class="content-2">
            <span class="number">45</span>
            <h3 class="white">Programas de RBC para personas con discapacidades en el Eje Cafetero.</h3>
        </div>
    </article>
    <article class="categoria alt square-7">
        <div class="content-2">
            <span class="number">1.250</span>
            <h3 class="white">Lideres de paz convivencia y reconciliacion y vigias de seguridad urbana y rural.</h3>
        </div>
    </article>
    <article class="categoria square-8">
        <div class="content">
            <span class="number">1.672</span>
            <h3 class="orange">Personas bancarizadas y con microcreditos</h3>
        </div>
    </article>
    <article class="categoria alt square-9">
        <div class="content-2">
            <span class="number">6.225</span>
            <h3 class="white">Personas Capacitadas para emprendimiento empresarial formación para el empleo y
                comercializacion sostenible a nivel nacional.</h3>
        </div>
    </article>
</section>