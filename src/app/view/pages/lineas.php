<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<section class="encausa lineas categorias">
    <?php
$cont = 1;
foreach ($lineasIntervencion as $linea) {

    if ($cont == 1) {
        $clase = 'alt';
        $cont = 0;
    } else if ($cont == 0) {
        $clase = '';
        $cont = $cont + 1;
    }
    ?>
    <a class="categoria <?=$clase?>" href="<?=get_route('public-blog-lineas', ["id_linea" => $linea->friendly_url])?>"
        style="background-image:url('<?=baseurl($linea->imagen)?>')">
        <div class="content">
            <span class="icon">
                <img src="<?=baseurl($linea->icon)?>">
            </span>
            <h3><?=$linea->nombre?></h3>
        </div>
        <div class="courtain"></div>

    </a>
    <?php
}
?>
</section>
