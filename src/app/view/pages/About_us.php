<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<section class="container about-us-container">
    <div class="contain">
        <h1>QUIENES SOMOS</h1>
        <p>
        Somos un grupo de profesionales de diversas disciplinas, con experiencia en el
        campo empresarial y social, que diseñamos, ejecutamos y asesoramos programas
        de responsabilidad social empresarial del sector público y privado, enfocados en
        trabajar con población vulnerable para generar ingresos, empleo digno y
        sostenibilidad de emprendimientos familiares y micro empresas comunitarias.
        Construimos Cultura de Paz, generamos espacios lúdicos, deportivos, de
        convivencia ciudadana y restablecimiento de los derechos.
        </p>

        <div class="two-fields">
            <div>
                <h3>MISIÓN</h3>
                <p>
                    Trabajar por el mejoramiento de la
                    calidad de vida de las personas en
                    condición de vulnerabilidad, a través
                    de la Inclusión social, laboral y de
                    Generación de ingresos sostenibles,
                    consolidando una Cultura de Paz.
                </p>
            </div>

            <div>
                <h3>VISIÓN</h3>
                <p>
                    Ser líderes en sostenibilidad,
                    desarrollo social y restablecimiento
                    de derechos
                </p>
            </div>
        </div>
    </div>
</section>

<section class="cover"></section>

<section class="purposes">
    <h3 class="title">PROPÓSITOS</h3>
    <div class="contain-p">
        <div>
            <h3 class="sub-title">INCLUSIÓN SOCIAL Y
            GENERACIÓN DE INGRESOS:</h3>
            <p>
                Desarrollamos programas de
                creación de unidades productivas
                sostenibles con fortalecimiento
                empresarial, Acompañamiento
                Psicosocial y comercialización,
                transversalizados por el monitoreo,
                seguimiento y control de calidad.
            </p>
        </div>
        <div>
            <h3 class="sub-title">
                ESTRATEGIA DE COMUNICACIÓN:
            </h3>
            <p>
                Diseñamos y ejecutamos planes
                de comunicación y visibilidad de
                los Proyectos sociales, a través de
                procesos: <b> Internos </b> imagen
                corporativa, piezas de publicidad,
                mercadeo y comercialización.
                <b> Externos </b>: Free press a nivel
                nacional, campañas en medios de
                comunicación, voceros y lideres de
                opinión, entre otras.
            </p>
        </div>
        <div>
            <h3 class="sub-title">
                ALIANZAS ESTRATEGICAS:
            </h3>
            <p>
                Gestionamos la economía
                sostenible para lograr objetivos
                direccionados en doble vía y
                converger con el sector público,
                privado y social, en beneficio de la
                población vulnerable y en
                situación de pobreza del país.
            </p>
        </div>
    </div>
</section>
