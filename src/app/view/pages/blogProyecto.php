<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<section class="loader">
    <ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</section>
<?php $fecha = explode(" ", $datosProyecto->fecha_reg)[0];
$fecha = explode("-", $fecha);
$fecha = "$fecha[2]-$fecha[1]-$fecha[0] ";
?>
<section class="carousel carouselProyectos">
	

    <img src="<?=baseurl()?><?=$datosProyecto->images?> " alt="cover">
    <div class="carousel-cont">
        <div>
			<h2 id="title"> <?=$datosProyecto->titulo?> </h2>
		
            <span> <span id="type"> </span> Blog | <?=$fecha?> <span id="date">
                </span> </span>
        </div>
    </div>
    <div class="blur"></div>
</section>

<section class="text">
    <div class="body-text" id="body_text">
		
        <?= $datosProyecto->descripcion?>
    </div>
    <div>
        <section class="cuadros categorias">
            <?php
if ($listadoNoticias) {
    foreach ($listadoNoticias as $noticia_proyecto) {

		$imageNew =json_decode($noticia_proyecto->imageNews);

		
        ?>
            <a href="<?=get_route('public-blog', ["friendly_url" => $noticia_proyecto->seourlNew])?>" class="categoria"
                style="background-image:url(<?=baseurl()?><?=$imageNew->imagePortada?>)">
                <div class="content">
                    <h3><?=$noticia_proyecto->title_new?></h3>
                </div>
                <div class="courtain"></div>

            </a>
            <?php
}
}
?>
        </section>
    </div>
</section>

<section class="more-news ">
    <div id="items" class="<?php if ($lastProject == '' || $nextProject == '') {echo 'one';}?>">
        <?php

if ($lastProject != '') {
    ?>
        <div>
            <div class="inverted">
                <div>
                    <h3>
                        <a class="title" href="blogProyecto/<?=$lastProject->seo_url?>">
                            Anterior
                            <div>
                                <h1><?=$lastProject->titulo?></h1>
                            </div>
                        </a>
                    </h3>
                    <label class="date" for="">Blog | <?=explode(" ", $lastProject->fecha_reg)[0]?></label>
                </div>
            </div>
        </div>
        <?php
}
if ($nextProject != '') {
    ?>
        <div>
            <div class="inverted">
                <div class="left">
                    <h3>
                        <a class="title" href="blogProyecto/<?=$nextProject->seo_url?>">
                            Siguiente
                            <div>
                                <h1><?=$nextProject->titulo?></h1>
                            </div>
                        </a>
                    </h3>
                    <label class="date" for="">Blog | <?=explode(" ", $nextProject->fecha_reg)[0]?></label>
                </div>
            </div>
        </div>
        <?php
}
?>
    </div>
</section>
