<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>

<section class="encausa proyectos categorias">

    <?php
foreach ($proyectos as $p) {
    ?>
    <a href="<?=get_route('public-blog-proyecto', ["url-friendly" => $p->seo_url])?>" class="categoria"
        style="background-image:url(<?=$p->images?>)">
        <div class="content">
            <h3><?=$p->titulo?></h3>
            <h4><?=$p->nombre?></h4>
        </div>
        <div class="courtain"></div>
    </a>
    <?php
}
?>

</section>
