<?php defined("BASEPATH") or die("<h1>El script no puede ser accedido directamente</h1>");?>
<section class="landing" style="background: url(<?=base_url("statics/images/$fondo")?>)">
    <div class="landing-text">
        <p>Trabajamos por la orientación vocacional en Formación para el empleo, Emprendimiento Empresarial y
            Empleabilidad.</p>
    </div>
</section>
<section class="resenna">
    <span class="linea-punto"></span>
    <div class="texto">
        <h1>FUNDACIÓN ENCAUSA</h1>
        <p> cuenta con un grupo de profesionales de diversas disciplinas, con experiencia en el campo
            empresarial y social que diseña, ejecuta y asesora programas de responsabilidad social empresarial del
            sector público y privado enfocados en trabajar con población vulnerable.</p>
        <a href="<?=get_route('public-about-us')?>" class="button">Conoce más</a>
    </div>
</section>

<section class="imagenes" id="articles" url="<?=get_route('public-load-data')?>">
    <?php

$opciones = [
    1 => 'titular',
    2 => 'titular-gray titular',
];

$array = array(1, 2, 3);
shuffle($array);
$cont = -1;
$what_color = 1;

foreach ($listNoticias as $noticia) {
    $cont = $cont + 1;
    if ($cont > 2) {
        $cont = 0;
        shuffle($array);
    }
    $numAleatorio = $what_color;
	$segundoAleatorio = 1;
	$noticiasImagenes=json_decode($noticia->images);



	
	

    $var2 = '<a onmouseover="cuadEncima2(' . $noticia->id_new . ',' . $segundoAleatorio . ')" onmouseout="cuadAbajo2(' . $noticia->id_new . ',' . $segundoAleatorio . ')" id="noti-' . $noticia->id_new . '" class="' . $opciones[$numAleatorio] . '" href="' . get_route('public-blog', ['friendly_url' => $noticia->seo_url]) . '">';
    $var2 .= '<span class="mas noti-' . $noticia->id_new . '"  >+</span>';
    $var2 .= '<div class="titulo noti-' . $noticia->id_new . '">';
    $var2 .= '<h3>' . $noticia->title_new . '</h3>';
    $var2 .= '</div>';
    $var2 .= '<div class="img" style="background-image: url(' .$noticiasImagenes->imagePortada.')"></div>';
    $var2 .= '</a>';
    $valor = $var2;
    echo $valor;

    $what_color = $what_color == 1 ? 2 : 1;
}
?>
</section>

<input type="hidden" value="<?=$cantidadNoticias?>" id="cantidadNoticias">
<div class="cargarMas" style="cursor:pointer">
    <a class="button" id="cargarmas">
        Cargar Más +
    </a>
</div>

<section class="aliados">
    <div class="titulo alt">
        <h2>Nuestros aliados</h2>
    </div>
    <div class="logos">
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
        <div>
            <img src="img-gen/400/400">
        </div>
    </div>
</section>

<section class="franja">
    <div class="texto">
        <h3>Nuestras</br>líneas de intervención</h3>
    </div>
    <div class="boton">
        <a href="<?=get_route('public-lineas')?>" class="button">CONOCE MÁS</a>
    </div>
</section>
