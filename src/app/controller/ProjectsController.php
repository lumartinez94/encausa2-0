<?php

/**
 * ProjectsController.php
 */

namespace App\Controller;

use App\Controller\AdminPanelController;
use App\Model\LineasMapper;
use App\Model\ProyectosMapper as MainMapper;
use App\Model\ProyectosMapper;
use App\Model\UsersModel;
use PiecesPHP\BuiltIn\Article\Mappers\ArticleContentMapper;
use PiecesPHP\Core\Helpers\Directories\DirectoryObject;
use PiecesPHP\Core\Forms\FileUpload;
use PiecesPHP\Core\Forms\FileValidator;
use PiecesPHP\Core\HTML\HtmlElement;
use PiecesPHP\Core\Roles;
use PiecesPHP\Core\Route;
use PiecesPHP\Core\RouteGroup;
use PiecesPHP\Core\Utilities\Helpers\DataTablesHelper;
use PiecesPHP\Core\Utilities\ReturnTypes\Operation;
use PiecesPHP\Core\Utilities\ReturnTypes\ResultOperations;
use Slim\Exception\NotFoundException;
use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

/**
 * ProjectsController.
 *
 * Controlador de artistas
 *
 * @package     App\Controller
 * @author      Juan Nuñez
 * @copyright   Copyright (c) 2018
 */

class ProjectsController extends AdminPanelController
{
	const FORMAT_DATETIME = 'd-m-Y h:i:s';
	const UPLOAD_DIR = 'general/projects';
    const UPLOAD_DIR_TMP = 'general/projects/tmp';

    /**
     * $prefixParentEntity
     *
     * @var string
     */
    protected static $prefixParentEntity = 'pages';
    /**
     * $prefixEntity
     *
     * @var string
     */
    protected static $prefixEntity = 'projects';
    /**
     * $prefixSingularEntity
     *
     * @var string
     */
    protected static $prefixSingularEntity = 'project';
    /**
     * $title
     *
     * @var string
     */
    protected static $title = 'Proyecto';
    /**
     * $pluralTitle
     *
     * @var string
     */
	protected static $pluralTitle = 'Proyectos';
	 /**
     * $uploadDir
     *
     * @var string
     */
    protected $uploadDir = '';
    /**
     * $uploadDir
     *
     * @var string
     */
    protected $uploadTmpDir = '';
    /**
     * $uploadDirURL
     *
     * @var string
     */
    protected $uploadDirURL = '';
    /**
     * $uploadDirTmpURL
     *
     * @var string
     */
    protected $uploadDirTmpURL = '';

    /**
     * __construct
     *
     * @return static
     */
    public function __construct()
    {
        parent::__construct(false); //No cargar ningún modelo automáticamente.

        $this->model = (new MainMapper)->getModel();
		set_title(self::$title . ' - ' . get_title());
		$this->uploadDir = append_to_url(get_config('upload_dir'), self::UPLOAD_DIR);
        $this->uploadTmpDir = append_to_url(get_config('upload_dir'), self::UPLOAD_DIR_TMP);
        $this->uploadDirURL = str_replace(base_url(), '', append_to_url(get_config('upload_dir_url'), self::UPLOAD_DIR));
        $this->uploadDirTmpURL = str_replace(base_url(), '', append_to_url(get_config('upload_dir_url'), self::UPLOAD_DIR_TMP));
    }

    /**
     * addForm
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function addForm(Request $request, Response $response, array $args)
    {
       

		set_custom_assets([
            base_url('statics/js/cropperHandler.js'),            
        ], 'js');


        $action = self::routeName('actions-add');
        $quill_proccesor_link = self::routeName('image-handler');
		$back_link = self::routeName('list');
		$options_lineas = array_to_html_options(LineasMapper::allForSelect(), null);

        $data = [];
        $data['action'] = $action;
        $data['back_link'] = $back_link;
        $data['options_lineas'] = $options_lineas;
        $data['quill_proccesor_link'] = $quill_proccesor_link;

		$data['title'] = self::$title;
        import_quilljs(['imageResize']);
		import_cropper();

        $this->render('panel/layout/header');
        $this->render('panel/' . self::$prefixParentEntity . '/' . self::$prefixSingularEntity . '/add-form', $data);
        $this->render('panel/layout/footer');
    }

    /**
     * editForm
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function editForm(Request $request, Response $response, array $args)
    {
        import_cropper();
        set_custom_assets([
            base_url('statics/js/cropperHandler.js'),
        ], 'js');
        import_quilljs(['imageResize']);

        $id = $request->getAttribute('id', null);
        $id = !is_null($id) && ctype_digit($id) ? (int) $id : null;

        $element = new MainMapper($id);

        if (!is_null($element->id)) {
            $options_lineas = array_to_html_options(LineasMapper::allForSelect(), $element->id_linea_intervencion);
            $action = self::routeName('actions-edit');
            $back_link = self::routeName('list');
            $quill_proccesor_link = self::routeName('image-handler');

            $data = [];
            $data['action'] = $action;
            $data['element'] = $element;
            $data['options_lineas'] = $options_lineas;
            $data['back_link'] = $back_link;
            $data['title'] = self::$title;
            $data['quill_proccesor_link'] = $quill_proccesor_link;

            $this->render('panel/layout/header');
            $this->render('panel/' . self::$prefixParentEntity . '/' . self::$prefixSingularEntity . '/edit-form', $data);
            $this->render('panel/layout/footer');
        } else {
            throw new NotFoundException($request, $response);
        }
    }

    /**
     * listView
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return void
     */
    public function listView(Request $request, Response $response, array $args)
    {

        $process_table = self::routeName('datatables');
        //$back_link = self::routeName();
        $back_link = get_route('admin');
        $add_link = self::routeName('forms-add');

        $data = [];
        $data['process_table'] = $process_table;
        $data['back_link'] = $back_link;
        $data['add_link'] = $add_link;
        $data['has_permissions_add'] = strlen($add_link) > 0;
        $data['title'] = self::$pluralTitle;

        $this->render('panel/layout/header');
        $this->render('panel/' . self::$prefixParentEntity . '/' . self::$prefixSingularEntity . '/list', $data);
        $this->render('panel/layout/footer');
    }

    /**
     * all
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function all(Request $request, Response $response, array $args)
    {

        if ($request->isXhr()) {

            $query = $this->model->select();

            $query->execute();

            return $response->withJson($query->result());
        } else {
            throw new NotFoundException($request, $response);
        }
    }

    /**
     * dataTables
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function dataTables(Request $request, Response $response, array $args)
    {

        if ($request->isXhr()) {

            $columns_order = [
                'id',
                'titulo',
                'status',
                'fecha_reg',
            ];

            $result = DataTablesHelper::process([
                'columns_order' => $columns_order,
                'mapper' => new MainMapper(),
                'request' => $request,
                'on_set_data' => function ($e) {

                    $buttonEdit = new HtmlElement('a', 'Editar');
                    $buttonEdit->setAttribute('class', "ui button green");
                    $buttonEdit->setAttribute('href', self::routeName('forms-edit', [
                        'id' => $e->id,
                    ]));

                    if ($buttonEdit->getAttributes(false)->offsetExists('href')) {
                        $href = $buttonEdit->getAttributes(false)->offsetGet('href');
                        if (strlen(trim($href->getValue())) < 1) {
                            $buttonEdit = '';
                        }
                    }

                    $mapper = new MainMapper($e->id);

                    return [
                        $mapper->id,
                        strlen($mapper->titulo) > 50 ? substr($mapper->titulo, 0, 50) . '...' : $mapper->titulo,
                        $mapper->status == 1 ? "Activo" : "Inactivo",
                        !is_null($mapper->fecha_reg) ? $mapper->fecha_reg->format('d-m-Y h:i:s') : '-',
                        (string) $buttonEdit,
                    ];
                },
            ]);

            return $response->withJson($result->getValues());
        } else {
            throw new NotFoundException($request, $response);
        }
    }

    /**
     * action
     *
     * Creación/Edición
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function action(Request $request, Response $response, array $args)
    {

        $id = $request->getParsedBodyParam('id', -1);
        $title = $request->getParsedBodyParam('title', null);
        $subtitle = $request->getParsedBodyParam('subtitle', null);
        $line = $request->getParsedBodyParam('line', null);
        $description = $request->getParsedBodyParam('description', null);
        $status = $request->getParsedBodyParam('status', null);
        $status = isset($status) ? 1 : 0;
        $user = $this->user;
        $user = (int) $user->id;
        $date = date(self::FORMAT_DATETIME);
		$folder = (new \DateTime)->format('Y/m/d/') . str_replace('.', '', uniqid());
      
        $is_edit = $id !== -1;

        $valid_params = !in_array(null, [
            $title,
            $subtitle,
            $line,
			$description,
			
        ]);

        $operation_name = $is_edit ? __('projectBackend', 'Editar Proyecto') : __('projectBackend', 'Agregar Proyecto');

        $result = new ResultOperations([
            new Operation($operation_name),
        ], $operation_name);

        $result->setValue('redirect', false);

		$error_parameters_message = __('projectBackend', 'Los parámetros recibidos son erróneos.');
        $unknow_error_message = __('projectBackend', 'El proyecto que intenta modificar no existe');
        $success_create_message = __('projectBackend', 'Proyecto creado.');
        $success_edit_message = __('projectBackend', 'Datos guardados.');
        $unknow_error_message = __('projectBackend', 'Ha ocurrido un error desconocido.');
        $is_duplicate_message = __('projectBackend', 'Ya existe un proyecto con ese nombre en la categoría seleccionada.');
        $not_exists_message = __('projectBackend', 'El idioma que intenta usar no existe.');

        $redirect_url_on_create = self::routeName('list');

        if ($valid_params) {

			$title = clean_string($title);
			$description=clean_string($description);
            $friendly_url = MainMapper::generateFriendlyURL($title, $id);
            $is_duplicate = MainMapper::isDuplicate($title, $friendly_url, $id);
            

            if (!$is_duplicate) {

                if (!$is_edit) {

                    $mapper = new MainMapper();

                    try {
                        $mapper->titulo = $title;
                        $mapper->subtitulo = $subtitle;
                        $mapper->id_linea_intervencion = $line;
                        $mapper->status = $status;
                        $mapper->descripcion = $description;
                        $mapper->seo_url = $friendly_url;
                        $mapper->folder = $folder;
                        $mapper->fecha_reg = $date;
						$mapper->id_user = $user;
					
						//Imagenes
						$imageProject=self::handlerUploadImage('portada',$folder);

						$mapper->images=$imageProject;

						$saved = $mapper->save();
					


                        if ($saved) {

			
                            $result->setMessage($success_create_message)
                                ->operation($operation_name)
                                ->setSuccess(true);

                            $result->setValue('redirect', true);

                            $result->setValue('redirect_to', $redirect_url_on_create);
                        } else {
                            $result->setMessage($unknow_error_message);
                        }
                    } catch (\Exception $e) {
						$result->setMessage($e->getMessage());
						$result->setValue('Exception', [
							'message' => $e->getMessage(),
							'code' => $e->getCode(),
							'line' => $e->getLine(),
							'file' => $e->getFile(),
						]);
                    }
                } else {

                    $mapper = new MainMapper((int) $id);
                    $exists = !is_null($mapper->id);

                    if ($exists) {

                        try {

                            $mapper->titulo = $title;
                            $mapper->subtitulo = $subtitle;
                            $mapper->id_linea_intervencion = $line;
                            $mapper->status = $status;
                            $mapper->descripcion = $description;
                            $mapper->seo_url = $friendly_url;
                            $mapper->fecha_reg = $date;
							$mapper->id_user = $user;
							
							//imagenes
							
							$imageProject= self::handlerUploadImage(
								"portada",
								$mapper->folder,
								$mapper->images
							);
							$mapper->images = strlen($imageProject)> 0 ? $imageProject : $mapper->images;
							
                            $updated = $mapper->update();
							
                            if ($updated) {
								
                                $result->setValue('reload', true);

                                $result->setMessage($success_edit_message)
                                    ->operation($operation_name)
                                    ->setSuccess(true);
                            } else {
                                $result->setMessage($unknow_error_message);
                            }
                        } catch (\Exception $e) {
							$result->setMessage($e->getMessage());
							$result->setValue('Exception', [
								'message' => $e->getMessage(),
								'code' => $e->getCode(),
								'line' => $e->getLine(),
								'file' => $e->getFile(),
							]);
                        }
                    } else {
                        $result->setMessage($not_exists_message);
                    }
                }
            } else {

                $result->setMessage($is_duplicate_message);
            }
        } else {
            $result->setMessage($error_parameters_message);
        }

        return $response->withJson($result);
	}
	 /**
     * handlerUploadImage
     *
     * @param string $nameOnFiles
     * @param string $folder
     * @param string $currentRoute
     * @param bool $setNameByInput
     * @return string
     */
    protected static function handlerUploadImage(string $nameOnFiles, string $folder, string $currentRoute = null, bool $setNameByInput = true)
    {
        $handler = new FileUpload($nameOnFiles, [
            FileValidator::TYPE_ALL_IMAGES,
        ]);
        $valid = false;
        $relativeURL = '';

        $name = 'file_' . uniqid();
        $oldFile = null;

        if ($handler->hasInput()) {

            try {

                $valid = $handler->validate();

                $uploadDirPath = (new static )->uploadDir;
                $uploadDirRelativeURL = (new static )->uploadDirURL;

                if ($setNameByInput && $valid) {

                    $name = $_FILES[$nameOnFiles]['name'];
                    $lastPointIndex = strrpos($name, '.');

                    if ($lastPointIndex !== false) {
                        $name = substr($name, 0, $lastPointIndex);
                    }

                }

                if (!is_null($currentRoute)) {
                    //Si ya existe
                    $oldFile = append_to_url(basepath(), $currentRoute);
                    $oldFile = file_exists($oldFile) ? $oldFile : null;
                }

                $uploadDirPath = append_to_url($uploadDirPath, $folder);
                $uploadDirRelativeURL = append_to_url($uploadDirRelativeURL, $folder);

                if ($valid) {

                    $locations = $handler->moveTo($uploadDirPath, $name, null, false, true);

                    if (count($locations) > 0) {

                        $url = $locations[0];
                        $nameCurrent = basename($url);
                        $relativeURL = trim(append_to_url($uploadDirRelativeURL, $nameCurrent), '/');

                        //Eliminar archivo anterior
                        if (!is_null($oldFile)) {

                            if (basename($oldFile) != $nameCurrent) {
                                unlink($oldFile);
                            }

                        }

                        //Se elimina cualquier otro archivo
                        foreach ($locations as $file) {
                            if ($url != $file) {
                                if (is_string($file) && file_exists($file)) {
                                    unlink($file);
                                }
                            }
                        }

                    }

                } else {
                    throw new \Exception(implode('<br>', $handler->getErrorMessages()));
                }

            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

        }

        return $relativeURL;
    }

    /**
     * quillImageHandler
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function quillImageHandler(Request $request, Response $response, array $args)
    {
        $files_uploaded = $request->getUploadedFiles();
        $image = isset($files_uploaded['image']) ? $files_uploaded['image'] : null;

        $result = new ResultOperations([
            'uploadImage' => new Operation('uploadImage'),
        ]);

        $result->setValue('path', null);

        if (!is_null($image)) {

            $images = is_array($image) ? $image : [$image];

            foreach ($images as $image) {

                if ($image->getError() === UPLOAD_ERR_OK) {

                    $filename = move_uploaded_file_to($this->uploadTmpDir, $image, uniqid());

                    $url = append_to_url(base_url($this->uploadDirTmpURL), $filename);

                    if (!is_null($filename)) {
                        $result
                            ->operation('uploadImage')
                            ->setMessage(__('projectBackend', 'Imagen subida'))
                            ->setSuccess(true);
                        $result->setValue('path', $url);
                    } else {
                        $result
                            ->operation('uploadImage')
                            ->setMessage(__('projectBackend', 'La imagen no pudo ser subida, intente después.'));
                    }
                }
            }
        } else {
            $result
                ->operation('uploadImage')
                ->setMessage(__('projectBackend', 'No se ha subido ninguna imagen.'));
        }

        return $response->withJson($result);
    }

    /**
     * moveTemporaryImages
     *
     * @param MainMapper $entity
     * @param string $oldText
     * @return void
     */
    protected function moveTemporaryImages(MainMapper &$entity, string $oldText = null)
    {
        $imagesOnText = [];
        $imagesOnOldText = [];
        $currentImagesOnText = [];

        $isEdit = !is_null($oldText) && strlen($oldText) > 0;
        $id = $entity->id;

        $regex = '/https?\:\/\/[^\",]+/i';

        preg_match_all($regex, $entity->descripcion, $imagesOnText);

        $imagesOnText = $imagesOnText[0];

        if (count($imagesOnText) > 0) {

            foreach ($imagesOnText as $url) {

                if (strpos($url, $this->uploadDirTmpURL) !== false) {

                    $filename = basename($url);

                    $oldPath = append_to_url($this->uploadTmpDir, "$filename");

                    $newFolder = append_to_url($this->uploadDir, "$id");

                    $newPath = append_to_url($newFolder, "$filename");

                    if (!file_exists($newFolder)) {
                        make_directory($newFolder);
                    }

                    if (file_exists($oldPath)) {
                        rename($oldPath, $newPath);
                    }

                    $_url = append_to_url($this->uploadDirURL, "$id/$filename");
                    $_url = trim($_url, '/');

                    $entity->descripcion = str_replace($url, $_url, $entity->descripcion);

                    $currentImagesOnText[] = $_url;
                } elseif (strpos($url, $this->uploadDirURL) !== false) {

                    $currentImagesOnText[] = trim($url, '/');
                }
            }
        }

        $updated = $entity->update();

        if ($isEdit) {

            preg_match_all($regex, $oldText, $imagesOnOldText);
            $imagesOnOldText = $imagesOnOldText[0];

            if ($updated && count($imagesOnOldText) > 0) {

                foreach ($imagesOnOldText as $url) {

                    if (!in_array($url, $currentImagesOnText)) {

                        $filename = str_replace($this->uploadDirURL, '', $url);

                        $path = append_to_url($this->uploadDir, $filename);

                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                }
            }
        }
    }

    /**
     * routeName
     *
     * @param string $name
     * @param array $params
     * @param bool $silentOnNotExists
     * @return string
     */
    public static function routeName(string $name = null, array $params = [], bool $silentOnNotExists = false)
    {

        if (!is_null($name)) {
            $name = trim($name);
            $name = strlen($name) > 0 ? "-{$name}" : '';
        }

        $name = !is_null($name) ? self::$prefixParentEntity . '-' . self::$prefixEntity . $name : self::$prefixParentEntity;

        $allowed = false;
        $current_user = get_config('current_user');

        if ($current_user != false) {
            $allowed = Roles::hasPermissions($name, (int) $current_user->type);
        } else {
            $allowed = true;
        }

        if ($allowed) {
            return get_route(
                $name,
                $params,
                $silentOnNotExists
            );
        } else {
            return '';
        }
    }

	/**
     * deleteOrphanFiles
     *
     * @return void
     */
    protected function deleteOrphanFiles()
    {
        $temporary_directory = new DirectoryObject($this->uploadTmpDir);
        $temporary_directory->process();
        $temporary_directory->delete();
    }

    /**
     * routes
     *
     * @param RouteGroup $group
     * @return RouteGroup
     */
    public static function routes(RouteGroup $group)
    {
        if (PIECES_PHP_BLOG_ENABLED) {

            $routes = [];

            $groupSegmentURL = $group->getGroupSegment();

            $lastIsBar = last_char($groupSegmentURL) == '/';
            $startRoute = $lastIsBar ? '' : '/';

            $roles_manage_permission = [
                UsersModel::TYPE_USER_ROOT,
                UsersModel::TYPE_USER_ADMIN,
            ];

            $group->active(PIECES_PHP_BLOG_ENABLED);
            $group->register($routes);

            //Rutas
            $group->register(
                self::genericManageRoutes($startRoute, self::$prefixParentEntity, self::class, self::$prefixEntity, $roles_manage_permission, true)
            );
        }

        return $group;
    }

    /**
     * genericManageRoutes
     *
     * @param string $startRoute
     * @param string $namePrefix
     * @param string $handler
     * @param string $uriPrefix
     * @param array $rolesAllowed
     * @return Route[]
     */
    protected static function genericManageRoutes(string $startRoute, string $namePrefix, string $handler, string $uriPrefix, array $rolesAllowed = [], bool $withQuillHandler = false)
    {
        $namePrefix .= '-' . $uriPrefix;
        $startRoute .= $uriPrefix;
        $all_roles = array_keys(UsersModel::TYPES_USERS);

        $routes = [
            new Route(
                "{$startRoute}",
                "{$handler}:all",
                "{$namePrefix}-ajax-all",
                'GET'
            ),
            new Route(
                "{$startRoute}/datatables[/]",
                "{$handler}:dataTables",
                "{$namePrefix}-datatables",
                'GET'
            ),
            new Route(
                "{$startRoute}/list[/]",
                "{$handler}:listView",
                "{$namePrefix}-list",
                'GET',
                true,
                null,
                $all_roles
            ),
            new Route(
                "{$startRoute}/forms/add[/]",
                "{$handler}:addForm",
                "{$namePrefix}-forms-add",
                'GET',
                true,
                null,
                $rolesAllowed
            ),
            new Route(
                "{$startRoute}/action/add[/]",
                "{$handler}:action",
                "{$namePrefix}-actions-add",
                'POST',
                true,
                null,
                $rolesAllowed
            ),
            new Route(
                "{$startRoute}/forms/edit/{id}[/]",
                "{$handler}:editForm",
                "{$namePrefix}-forms-edit",
                'GET',
                true,
                null,
                $rolesAllowed
            ),
            new Route(
                "{$startRoute}/action/edit[/]",
                "{$handler}:action",
                "{$namePrefix}-actions-edit",
                'POST',
                true,
                null,
                $rolesAllowed
            ),
        ];

        if ($withQuillHandler) {
            $routes[] = new Route(
                "{$startRoute}/quill-image-upload[/]",
                "{$handler}:quillImageHandler",
                "{$namePrefix}-image-handler",
                'POST',
                true,
                null,
                $rolesAllowed
            );
        }

        return $routes;
    }
}
