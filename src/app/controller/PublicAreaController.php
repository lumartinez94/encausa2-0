<?php

/**
 * PublicAreaController.php
 */

namespace App\Controller;

use App\Model\AvatarModel;
use App\Model\HomeMapper;
use App\Model\LineasMapper;
use App\Model\NewsMapper;
use App\Model\ProyectosMapper;
use PiecesPHP\Core\BaseHashEncryption;
use PiecesPHP\Core\Forms\FileUpload;
use PiecesPHP\Core\Forms\FileValidator;
use PiecesPHP\Core\Roles;
use PiecesPHP\Core\Route;
use PiecesPHP\Core\RouteGroup;
use PiecesPHP\Core\Utilities\ExifHelper;
use PiecesPHP\Core\Utilities\OsTicket\OsTicketAPI;
use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

/**
 * PublicAreaController.
 *
 * Controlador del área pública
 *
 * @package     App\Controller
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2019
 */
class PublicAreaController extends \PiecesPHP\Core\BaseController
{

    /**
     * $prefixNameRoutes
     *
     * @var string
     */
    private static $prefixNameRoutes = 'public';

    /**
     * $startSegmentRoutes
     *
     * @var string
     */
    private static $startSegmentRoutes = '';

    /**
     * $automaticImports
     *
     * @var bool
     */
    private static $automaticImports = true;

    /**
     * $user
     *
     * Usuario logueado
     *
     * @var \stdClass
     */
    protected $user = null;

    /**
     * __construct
     *
     * @return static
     */
    public function __construct()
    {
        parent::__construct(false); //No cargar ningún modelo automáticamente

        $this->init();

        if (self::$automaticImports === true) {

            /* JQuery */
            import_jquery();
            /* NProgress */
			import_nprogress();
			  /* Semantic */
			 
			
            /* Librerías de la aplicación */
			import_app_libraries();
			add_global_asset(
				base_url('statics/css/style.css'),

				'css');

            add_global_asset(
				base_url('statics/css/global.css')
				, 'css');

				add_global_asset(
					baseurl('statics/js/global.js')
					, 'js');
        }
    }

    /**
     * indexView
     *
     * Vista principal
     *
     * @param Request $req
     * @param Response $res
     * @param array $args
     * @return void
     */
    public function indexView(Request $req, Response $res, array $args)
    {
		import_quilljs();
        import_cropper();
        import_izitoast();
		  //import_swal2();
		  set_custom_assets([
            base_url('statics/css/j_changes.css'),
        ], 'css');
        set_custom_assets([
            baseurl('statics/js/loader.js'),
            baseurl('statics/js/articulos.js'),
        ], 'js');
		
		set_title("Inicio");
        //Fondos Index
        $fondo = HomeMapper::SelectWhere();

        //Noticias
        $data['listNoticias'] = NewsMapper::noticiasActivasIndex();

        $count = NewsMapper::cantidadNoticias();
        $fondo = rand(1, 3);
        $fondo = "fondo$fondo.jpg";

        $data['dataIndex'] = HomeMapper::all();
        $data['fondo'] = $fondo;
        $data['header'] = true;
        $data['is_home'] = true;
        $data['view'] = 'home';
		$data['cantidadNoticias'] = $count;
		$this->setVariables($data);

		$this->render('layout/header');
		$this->render('layout/menu');
		$this->render('pages/Home');
		$this->render('layout/footer');

        return $res;
	}
	
    public function balanceView(Request $req, Response $res, array $args)
    {
        set_custom_assets([
            baseurl('statics/css/encausa.css'),
            base_url('statics/css/j_changes.css'),

        ], 'css');

        $data['dataIndex'] = HomeMapper::all();
        $data['fondo'] = HomeMapper::SelectWhere();
        set_title("Balance Social");

        $data['view'] = 'balance';
        $data['titulo'] = 'Balance Social';
        $data['descripcion'] = 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Praesentium ad ullam rem reprehenderit nostrum fugiat facilis libero laudantium, commodi vel molestias numquam magni? Quis animi id obcaecati temporibus quidem facilis.Lorem, ipsum dolor sit amet consectetur adipisicing elit. Praesentium ad ullam rem reprehenderit nostrum fugiat facilis libero laudantium, commodi vel molestias numquam magni? Quis animi id obcaecati temporibus quidem facilis.    ';

        $this->setVariables($data);
        $this->render('layout/menu');
        $this->render('layout/header-internas');
        $this->render("pages/Balance");
        $this->render('layout/footer');
        return $res;
    }
	
    public function blogNoticiasView(Request $req, Response $res, array $args)
    {
        //import_swal2();
        set_custom_assets([
            baseurl('statics/css/blog.css'),
            base_url('statics/css/j_changes.css'),
        ], 'css');

        set_custom_assets([
            baseurl('statics/js/plugins/moment.js'),
            baseurl('statics/js/loader.js'),
        ], 'js');

        set_title("Blog Noticias");

        $data['dataIndex'] = HomeMapper::all();
        $data['fondo'] = HomeMapper::SelectWhere();
        $data['datosNoticias'] = NewsMapper::innerNoticias($args['friendly_url']);

        if (isset($data['datosNoticias']->id_new)) {
            $idNew = $data['datosNoticias']->id_new;

            $cantidadVisitas = $data['datosNoticias']->visits;
            NewsMapper::contadorVisitas($idNew, $cantidadVisitas);

        } else {
            $idNew = '';
        }
        //Noticias Anteriores y siguientes
        $anterior = NewsMapper::lastNew($idNew);
        $siguiente = NewsMapper::nextNew($idNew);

        if (isset($anterior) && $anterior != '-1') {
            $data['lastNew'] = $anterior;
            $fechaCreacionAnt = explode(' ', $data['lastNew']->created_at);
            $data['lastNew']->created_at = $fechaCreacionAnt[0];
        } else {
            $data['lastNew'] = '';
        }

        if (isset($siguiente) && $siguiente != '-1') {
            $data['nextNew'] = $siguiente;
            $fechaCreacionSig = explode(' ', $data['nextNew']->created_at);
            $data['nextNew']->created_at = $fechaCreacionSig[0];
        } else {
            $data['nextNew'] = '';
        }

        $data['view'] = 'home';
        $data['is_home'] = false;
        $data['idem'] = $args['friendly_url'];
        $this->setVariables($data);

        $this->render('layout/header', $data);
        $this->render('layout/menu');
        $this->render('pages/blogNoticias', $data);
        $this->render('layout/footer', $data);

        return $res;
	}
	
	public function BlogLineasIntervencion(Request $req, Response $res, array $args)
    {
        //import_swal2();
        set_custom_assets([
            baseurl('statics/css/blog.css'),
            base_url('statics/css/j_changes.css'),
        ], 'css');
        set_custom_assets([
            baseurl('statics/js/plugins/moment.js'),
            baseurl('statics/js/loader.js'),
        ], 'js');
        set_title("Blog Proyectos");

        $datosLineaIntervencion = LineasMapper::datosLineaIntervencion($args['id_linea']);
        $data['datosLineaIntervencion'] = $datosLineaIntervencion;

        //Proyectos que contiene la linea de intervencion
        $Proyectos_LineaIntervencion = LineasMapper::innerLineasIntervencionProyectos($datosLineaIntervencion->id);
        $data['Proyectos_LineaIntervencion'] = $Proyectos_LineaIntervencion;

        $data['view'] = 'lineas-intervencion';
        $data['is_home'] = false;
        $data['titulo'] = 'Líneas de intervención';
        $this->setVariables($data);

        $this->render('layout/header', $data);
        $this->render('layout/menu');
        $this->render("pages/Blog_lineas", $data);
        $this->render('layout/footer', $data);

        return $res;
    }

    public function aboutUsView(Request $req, Response $res, array $args)
    {

        set_custom_assets([
            baseurl('statics/css/about_us.css'),
            base_url('statics/css/j_changes.css'),

        ], 'css');
        set_title("Quienes Somos");

        $data['dataIndex'] = HomeMapper::all();
        $data['fondo'] = HomeMapper::SelectWhere();
        $data['view'] = 'about_us';
        $data['is_home'] = false;
        $this->setVariables($data);

        $this->render('layout/header', $data);
        $this->render('layout/menu');
        $this->render('pages/About_us', $data);
        $this->render('layout/footer', $data);

        return $res;
    }

	public function lineasView(Request $req, Response $res, array $args)
    {
        set_custom_assets([
            baseurl('statics/css/encausa.css'),
            base_url('statics/css/j_changes.css'),

        ], 'css');
        set_title("Lineas de Intervencion");

        $data['dataIndex'] = HomeMapper::all();
        $data['fondo'] = HomeMapper::SelectWhere();

        //Lineas de intervencion
        $lineasIntervencion = LineasMapper::allData();
        $data['lineasIntervencion'] = $lineasIntervencion;

        $data['view'] = 'lineas-intervencion';
        $data['titulo'] = 'Líneas de intervención';
        $data['descripcion'] = 'Nuestros programas de construcción e intervención.';
        //$data['current'] = 'lineas';
        $this->setVariables($data);

        $this->render('layout/menu');
        $this->render('layout/header-internas');
        $this->render("pages/lineas");
        $this->render('layout/footer');

        return $res;
    }

	public function proyectosView(Request $req, Response $res, array $args)
    {

        set_custom_assets([
            baseurl('statics/css/encausa.css'),
            base_url('statics/css/j_changes.css'),

        ], 'css');

        $data['dataIndex'] = HomeMapper::all();
        $data['fondo'] = HomeMapper::SelectWhere();
        $data['proyectos'] = ProyectosMapper::innerProyectosLineas();

        $data['view'] = 'proyectos';
        $data['titulo'] = 'Proyectos';
        $data['descripcion'] = 'Nuestros proyectos.';

        set_title("Proyectos");

        $this->setVariables($data);

        $this->render('layout/menu');
        $this->render('layout/header-internas');
        $this->render("pages/proyectos");
        $this->render('layout/footer');

        return $res;
	}

	public function blogProyecto(Request $req, Response $res, array $args)
    {
        //import_swal2();
        set_custom_assets([
            baseurl('statics/css/blog.css'),
            base_url('statics/css/j_changes.css'),

        ], 'css');
        set_custom_assets([
            baseurl('statics/js/plugins/moment.js'),
            baseurl('statics/js/loader.js'),
        ], 'js');
        set_title("Blog Proyectos");

        $data['dataIndex'] = HomeMapper::all();
        $data['fondo'] = HomeMapper::SelectWhere();

        //datos del proyecto
        $data['datosProyecto'] = ProyectosMapper::innerProyectos($args['url-friendly']);

        if (isset($data['datosProyecto']->id)) {
            $id = $data['datosProyecto']->id;

        } else {
            $id = '';
        }

        //$userData = $this->UsersModel->getByID($data['datosProyecto']->id_user);

        //Proyectos Anteriores y siguientes
        $anterior = ProyectosMapper::lastProject($id);
        $siguiente = ProyectosMapper::nextProject($id);

        if (isset($anterior) && $anterior != '-1') {
            $data['lastProject'] = $anterior;
        } else {
            $data['lastProject'] = '';
        }

        if (isset($siguiente) && $siguiente != '-1') {
            $data['nextProject'] = $siguiente;
        } else {
            $data['nextProject'] = '';
        }

        //listado de noticias por proyectos
        $data['listadoNoticias'] = ProyectosMapper::listaNoticiasPorProyecto($args['url-friendly']);

        $data['is_home'] = false;
        $data['idem'] = $args['url-friendly'];
        $data['view'] = 'proyectos';
        //$data['userData'] = $userData;
        $this->setVariables($data);

        $this->render('layout/header', $data);
        $this->render('layout/menu');
        $this->render("pages/blogProyecto", $data);
        $this->render('layout/footer', $data);

        return $res;
    }

    //extra

    public function cargarDataIndex(Request $req, Response $res, array $args)
    {

        //contador automatico
        $contador = $req->getParsedBodyParam('contador');
        //cantidad de noticias en general
        $cantidadNoticias = NewsMapper::cantidadNoticias();
        $count = $cantidadNoticias;

        //id anterior
        $anteriorId = NewsMapper::noticiaAnterior($contador);

        $opciones = [
            1 => 'titular',
            2 => 'titular-gray titular',
        ];

        $array = array(1, 2, 3);
        shuffle($array);
        $cont = -1;
        $what_color = 2;
        $valor = "";
        foreach ($anteriorId as $noticia) {
            $cont = $cont + 1;
            if ($cont > 2) {
                $cont = 0;
                shuffle($array);
            }
            $numAleatorio = $what_color;
            $segundoAleatorio = 1;

            $var2 = '<a onmouseover="cuadEncima2(' . $noticia->id_new . ',' . $segundoAleatorio . ')" onmouseout="cuadAbajo2(' . $noticia->id_new . ',' . $segundoAleatorio . ')" id="noti-' . $noticia->id_new . '" class="' . $opciones[$numAleatorio] . '" href="' . get_route('public-blog', ['friendly_url' => $noticia->seo_url]) . '">';
            $var2 .= '<span class="mas noti-' . $noticia->id_new . '"  >+</span>';
            $var2 .= '<div class="titulo noti-' . $noticia->id_new . '">';
            $var2 .= '<h3>' . $noticia->title_new . '</h3>';
            $var2 .= '</div>';

            $var2 .= '<div class="img" style="background-image: url(' . $noticia->images->imagePortada.'"></div>';

            $var2 .= '</a>';
            $valor .= $var2;

            $what_color = $what_color == 1 ? 2 : 1;
        }
        echo $valor . '|' . $contador . '|' . $count;

        //die();
    }

    /**
     * routeName
     *
     * @param string $name
     * @param array $params
     * @param bool $silentOnNotExists
     * @return string
     */
    public static function routeName(string $name = null, array $params = [], bool $silentOnNotExists = false)
    {
        if (!is_null($name)) {
            $name = trim($name);
            $name = strlen($name) > 0 ? "-{$name}" : '';
        }

        $name = !is_null($name) ? self::$prefixNameRoutes . $name : self::$prefixNameRoutes;

        $allowed = false;
        $current_user = get_config('current_user');

        if ($current_user != false) {
            $allowed = Roles::hasPermissions($name, (int) $current_user->type);
        } else {
            $allowed = true;
        }

        if ($allowed) {
            return get_route(
                $name,
                $params,
                $silentOnNotExists
            );
        } else {
            return '';
        }
    }

    /**
     * routes
     *
     * @param RouteGroup $group
     * @return RouteGroup
     */
    public static function routes(RouteGroup $group)
    {

        $groupSegmentURL = $group->getGroupSegment();

        $lastIsBar = last_char($groupSegmentURL) == '/';
        $startRoute = $lastIsBar ? '' : '/';

        //Otras rutas
        $namePrefix = self::$prefixNameRoutes;
        $startRoute .= self::$startSegmentRoutes;

        //──── GET ─────────────────────────────────────────────────────────────────────────

        //Generales
        $group->register([
            new Route(
                $lastIsBar ? "" : "[/]",
                self::class . ":indexView",
                "{$namePrefix}-index",
				'POST|GET'
			),
            new Route(
                "{$startRoute}balance-social[/]",
                self::class . ":balanceView",
                "{$namePrefix}-balance",
                'GET'
            ),
            new Route(
                "{$startRoute}acerca-de-nosotros[/]",
                self::class . ":aboutUsView",
                "{$namePrefix}-about-us",
                'GET'
            ),
            new Route(
                "{$startRoute}lineas-intervencion[/]",
                self::class . ":lineasView",
                "{$namePrefix}-lineas",
                'GET'
            ),
            new Route(
                "{$startRoute}proyectos[/]",
                self::class . ":proyectosView",
                "{$namePrefix}-proyectos",
                'GET'
            ),
            new Route(
                "{$startRoute}blog/{friendly_url}[/]",
                self::class . ":blogNoticiasView",
                "{$namePrefix}-blog",
                'GET'
            ),
            new Route(
                "{$startRoute}blogProyecto/{url-friendly}[/]",
                self::class . ":blogProyecto",
                "{$namePrefix}-blog-proyecto",
                'GET'
            ),
            new Route(
                "{$startRoute}cargarDataIndex[/]",
                self::class . ":cargarDataIndex",
                "{$namePrefix}-load-data",
                'POST'
            ),
            new Route(
                "{$startRoute}blog-lineas-intervencion/{id_linea}[/]",
                self::class . ":BlogLineasIntervencion",
                "{$namePrefix}-blog-lineas",
                'GET'
			),
			
        ]);

        //──── POST ─────────────────────────────────────────────────────────────────────────

        return $group;
    }

    /**
     * init
     *
     * @return void
     */
    protected function init()
    {
        $api_url = get_config('osTicketAPI');
        $api_key = get_config('osTicketAPIKey');

        OsTicketAPI::setBaseURL($api_url);
        OsTicketAPI::setBaseAPIKey($api_key);

        $view_data = [];
        $this->user = get_config('current_user');

        if ($this->user instanceof \stdClass) {
            $view_data['user'] = $this->user;
            $this->user->avatar = AvatarModel::getAvatar($this->user->id);
            $this->user->hasAvatar = !is_null($this->user->avatar);
            $this->user->id = BaseHashEncryption::encrypt(base64_encode($this->user->id), self::class);
            unset($this->user->password);
        }

		$this->setVariables($view_data);
		
		/* JQuery */
 		import_jquery();
		 //import_app_libraries();
		 
    }
}
