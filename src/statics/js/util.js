function Util() {

    /**
     * @description Variables privadas de la clase
     */
    var private = {
        /**
         * @property nodosEliminar
         * @description Nodos que se eliminarán con clearTextNodes()
         * @type {Array}
         */
        nodosEliminar: []
    }

    /**
     * @method formatDate
     * 
     * Formatea una fecha.
     * 
     * @param {Date} date Fecha
     * @param {String} format Formato de la fecha y = año, m = mes, d = día
     */
    function formatDate(date, format) {

        format = typeof format === 'undefined' ? 'y-m-d' : format

        var day = date.getDate()
        day = day < 10 ? '0' + day.toString() : day

        var month = date.getMonth() + 1
        month = month < 10 ? '0' + month.toString() : month

        var year = date.getFullYear()

        format = format.replace('y', year)
        format = format.replace('m', month)
        format = format.replace('d', day)

        return format
    }

    /**
     * @method reformatStringDate
     * 
     * Reformatea una fecha.
     * 
     * @param {String} dateString String que representa la fecha
     * @param {String} format Formato actual del string de la fecha y = año, m = mes, d = día
     * @param {String} separator Separador actual del string de la fecha
     * @param {String} newFormat Formato deseado del string de la fecha y = año, m = mes, d = día
     */
    function reformatStringDate(dateString, format, separator, newFormat) {

        format = typeof format === 'undefined' ? 'y-m-d' : format
        separator = typeof separator === 'undefined' ? '-' : separator
        newFormat = typeof newFormat === 'undefined' ? 'y-m-d' : newFormat

        var dateParts = dateString.split(separator)
        var formatParts = format.split(separator)

        var year = dateParts[formatParts.indexOf('y')]
        var month = dateParts[formatParts.indexOf('m')]
        var day = dateParts[formatParts.indexOf('d')]

        newFormat = newFormat.replace('y', year)
        newFormat = newFormat.replace('m', month)
        newFormat = newFormat.replace('d', day)

        return newFormat
    }

    /**
     * @method getMilisegundos
     * 
     * Retorna la cantidad en milisegundos del número proporcionado
     * según la magnitud escogida.
     * s = segundos
     * m = minutos
     * h = horas
     * 
     * @param {Number} numero El número que representa la cantidad deseada según la magnitud escogida
     * @param {String} magnitud 's'|'m'|'h' (Si se escoge un valor diferente por defecto asignara segundos)
     * @returns {Number} La cantidad en milisegundos
     */
    function getMilisegundos(numero, magnitud) {

        numero = typeof numero === 'undefined' ? 1 : numero
        magnitud = typeof magnitud === 'undefined' ? 's' : magnitud

        var milisegundos = numero * 1000
        switch (magnitud) {
            case 's':
                return milisegundos
                break
            case 'm':
                return milisegundos * 60
                break
            case 'h':
                return milisegundos * 60 * 60
                break
            default:
                return milisegundos
                break
        }
    }

    /**
     * @method mimeType
     * 
     * Valida el tipo de un archivo
     * 
     * Tipos implementados para el parámetro allowed:
     * xlsx, pdf, html, jpg, jpeg, png
     * 
     * @param {String} dataURL DataURL leído por FileReader 
     * @param {Array} allowed Array con los formatos permitidos
     * @param {Boolean} verbose Mensajes de información
     * @returns {Boolean} Devuelve true si el tipo del archivo pasado corresponde a algunos de los definidos
     * en allowed, de lo contrario devuelve false.
     */
    function mimeType(dataURL, allowed, verbose) {

        dataURL = typeof dataURL === 'undefined' ? '' : dataURL
        allowed = typeof allowed === 'undefined' ? [] : allowed
        verbose = typeof verbose === 'undefined' ? false : verbose

        var mimeSignatures = {
            xlsx: {
                string: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                extension: '.xlsx',
                signatures: [
                    arrayToString(stringToBytes('PK..').hex, ('PK..').length)
                ]
            },
            pdf: {
                string: 'application/pdf',
                extension: '.pdf',
                signatures: [
                    arrayToString(stringToBytes('%PDF-').hex, ('%PDF-').length)
                ]
            },
            html: {
                string: 'text/html',
                extension: '.html',
                signatures: [
                    arrayToString(stringToBytes('<!DOCTYPE HTML').hex, ('<!DOCTYPE HTML').length)
                ]
            },
            jpg: {
                string: 'image/jpeg',
                extension: '.jpg',
                signatures: [
                    arrayToString(stringToBytes('ÿØÿà ..J').hex, ('ÿØÿà ..J').length),
                    arrayToString(stringToBytes('F IF..').hex, ('F IF..').length)
                ]
            },
            jpeg: {
                string: 'image/jpeg',
                extension: '.jpeg',
                signatures: [
                    arrayToString(stringToBytes('ÿØÿà ..J').hex, ('ÿØÿà ..J').length),
                    arrayToString(stringToBytes('F IF..').hex, ('F IF..').length)
                ]
            },
            png: {
                string: 'image/png',
                extension: '.png',
                signatures: [
                    arrayToString(['89', '70', '6e', '67', '1a', '0'], ['89', '70', '6e', '67', '1a', '0'].length)
                ]
            },
        }

        var data = proccessDataURL(dataURL)
        var bytes = stringToBytes(data.string).hex
        for (var i in mimeSignatures) {
            var mime = mimeSignatures[i]
            var signatures = mime.signatures
            for (var j in signatures) {
                var signature = signatures[j]
                var header = arrayToString(bytes, signature.length)
                if (verbose === true) {
                    var verboseStrings = []

                    verboseStrings[0] = "Comparing headers '${header}' == ${signature} result: " + (header == signature)
                    verboseStrings[0] = verboseStrings[0].replace('${header}', header).replace('${signature}', signature)

                    verboseStrings[1] = "Comparing mime type '${data.mime}' == ${mime.string} result: " + (data.mime == mime.string)
                    verboseStrings[1] = verboseStrings[1].replace('${data.mime}', data.mime).replace('${mime.string}', mime.string)

                    verboseStrings[2] = 'Valid: ' + (header == signature && data.mime == mime.string)

                    console.log(verboseStrings)
                }
                if (header == signature && data.mime == mime.string) {
                    if (allowed.indexOf(i) > -1) {
                        return true
                    }
                }
            }
        }
        return false
    }


    /**
     * @method proccessDataURL
     * 
     * Devuelve un objeto con el archivo en base64, el tipo mime y el dataURL recibido.
     * 
     * @param {String} dataURL Data url de un archivo 
     * @returns {Object}
     */
    function proccessDataURL(dataURL) {
        var mimeType = dataURL.replace(/data\:(.*)\;base64\,.*/g, '$1')
        var fileString = window.atob(dataURL.replace(/data\:.*base64\,(.*)/g, '$1'))
        return {
            b64: dataURL,
            mime: mimeType,
            string: fileString
        }
    }

    /**
     * @method stringClear
     * 
     * Elimina espacios y pasa todo a minúsculas.
     * 
     * @param {String} str 
     * @returns {String} 
     */
    function stringClear(str) {
        return (new String(str).replace(/\s*/g, ''))
    }

    /**
     * @method stringLowerClear
     * 
     * Elimina espacios y pasa todo a minúsculas.
     * 
     * @param {String} str 
     * @returns {String} 
     */
    function stringLowerClear(str) {
        return stringClear(str).toLowerCase()
    }

    /**
     * @method trim
     * 
     * Elimina espacios y pasa todo a minúsculas.
     * 
     * @param {String} str 
     * @returns {String} 
     */
    function trim(str) {
        return (new String(str).trim())
    }

    /**
     * @method stringToBytes
     * 
     * Devulve un objeto con arrays de los códigos en DEC y en HEX de cada
     * carácter de una cadena previamente transformado en minúsculas.
     * 
     * @param {String} string 
     * @param {Boolean} verbose
     * @returns {Object} 
     */
    function stringToBytesToLower(string, verbose) {

        verbose = typeof verbose === 'undefined' ? false : verbose

        string = string.toString().replace(/(\r|\n|\t)*/gim, '').trim().toLowerCase()
        var lengthString = string.length
        var bytesDec = []
        var bytesHex = []

        if (verbose) console.log('============')
        for (var i = 0; i < lengthString; i++) {
            var charCode = string.charCodeAt(i)
            bytesDec[i] = charCode
            bytesHex[i] = charCode.toString(16)
            if (verbose) console.log(string[i] + '===' + charCode + '===' + bytesHex[i])
        }
        if (verbose) console.log('============')
        return { dec: bytesDec, hex: bytesHex }
    }

    /**
     * @method stringToBytes
     * 
     * Devulve un objeto con arrays de los códigos en DEC y en HEX de cada
     * carácter de una cadena. Elimina \r, \n, \t y espacios sobrantes a
     * la cadena de entrada.
     * 
     * @param {String} string 
     * @param {Boolean} verbose
     * @returns {Object} 
     */
    function stringToBytes(string, verbose) {

        verbose = typeof verbose === 'undefined' ? false : verbose

        string = string.toString().replace(/(\r|\n|\t)*/gim, '').trim()
        var lengthString = string.length
        var bytesDec = []
        var bytesHex = []

        if (verbose) console.log('============')
        for (var i = 0; i < lengthString; i++) {
            var charCode = string.charCodeAt(i)
            bytesDec[i] = charCode
            bytesHex[i] = charCode.toString(16)
            if (verbose) console.log(string[i] + '===' + charCode + '===' + bytesHex[i])
        }
        if (verbose) console.log('============')
        return { dec: bytesDec, hex: bytesHex }
    }

    /**
     * @method stringToBytesUnclear
     * 
     * Devulve un objeto con arrays de los códigos en DEC y en HEX de cada
     * carácter de una cadena. No modifica la cadena de entrada.
     * 
     * @param {String} string 
     * @param {Boolean} verbose
     * @returns {Object} 
     */
    function stringToBytesUnclear(string, verbose) {
        string = string.toString()
        verbose = verbose !== undefined ? verbose : false
        var lengthString = string.length
        var bytesDec = []
        var bytesHex = []

        if (verbose) console.log('============')
        for (var i = 0; i < lengthString; i++) {
            var charCode = string.charCodeAt(i)
            bytesDec[i] = charCode
            bytesHex[i] = charCode.toString(16)
            if (verbose) console.log(string[i] + '===' + charCode + '===' + bytesHex[i])
        }
        if (verbose) console.log('============')
        return { dec: bytesDec, hex: bytesHex }
    }

    /**
     * @method arrayToString
     * 
     * Toma un array y lo convierte en una cadena
     * con la extensión determinada.
     * 
     * @param {Array} array Array a convertir
     * @param {Number} length Tamaño de la cadena retornada
     * @returns {String}
     */
    function arrayToString(array, length) {
        var string = ''
        var arrayLength = array.length
        length = arrayLength >= length ? length : arrayLength
        for (var i = 0; i < arrayLength; i++) {
            string += array[i]
        }
        string = string.substring(0, length)
        return string.substring(0, length)
    }

    /**
     * @method getURIPart
     * 
     * @description Obtiene una parte de la URI actual
     * 
     * @param {Number} part El segmento deseado de la URI actual (Si es 0, devolverá la última parte)
     * 
     * @returns {String} El segmento deseado
     */
    function getURIPart(part) {

        part = typeof part === 'undefined' ? 0 : part

        var split_url = window.location.pathname.split('/')
        split_url = (function () {
            var tmp = []
            for (var i = 0; i < split_url.length; i++) {
                if (split_url[i].length > 0) {
                    tmp.push(split_url[i])
                }
            }
            return tmp
        })()
        var partes = split_url.length
        part = part <= partes ? (part === 0 ? partes - 1 : part - 1) : partes - 1
        return split_url[part]
    }


    /**
     * @method clearTextNodes
     * 
     * @description Elimina los nodos de texto vacíos y comentarios
     * 
     * @return {void}
     */
    function clearTextNodes() {
        var elementos = document.getElementsByTagName('*');
        for (var k = 0; k < elementos.length; k++) {
            for (var i = 0; i < elementos[k].childNodes.length; i++) {
                var hijo = elementos[k].childNodes[i];
                if ((hijo.nodeType == 3 && !/\S/.test(hijo.nodeValue)) || (hijo.nodeType == 8)) {
                    private.nodosEliminar[private.nodosEliminar.length] = hijo
                }
            }
        }
        for (var d = 0; d < private.nodosEliminar.length; d++) {
            private.nodosEliminar[d].parentNode.removeChild(private.nodosEliminar[d])
        }
    }

    return {
        clearTextNodes: clearTextNodes,
        string: {//Funciones auxiliares para cadenas y afines
            stringClear: stringClear,
            stringLowerClear: stringLowerClear,
            trim: trim,
            stringToBytes: stringToBytes,
            stringToBytesToLower: stringToBytesToLower,
            stringToBytesUnclear: stringToBytesUnclear,
            arrayToString: arrayToString,
            proccessDataURL: proccessDataURL,
        },
        uri: {//Funciones auxiliares para URIs
            getURIPart: getURIPart,
        },
        number: {//Funciones auxiliares para números y afines
            getMilisegundos: getMilisegundos,
        },
        date: {//Funciones auxiliares para fecha
            formatDate: formatDate,
            reformatStringDate: reformatStringDate,
        },
        validate: {//Funciones para validaciones
            mimeType: mimeType,
        },
    }
}