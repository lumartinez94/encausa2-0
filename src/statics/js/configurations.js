/**
 * Datos accesibles globalmente
 * @namespace
 */
var globales = {}

/**
 * Configuración de los calendarios
 * 
 * @property configCalendar
 * @type {Object}
 */
globales.configCalendar = {
    type: 'date',
    formatter: {
        date: function (date, settings) {
            if (!date) return ''
            return Util().date.formatDate(date, 'd-m-y')
        }
    },
    text: {
        days: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy',
        now: 'Ahora',
        am: 'AM',
        pm: 'PM'
    }
}

/**
 * Configuración de las tablas DataTables
 * 
 * @property configCalendar
 * @type {Object}
 */
globales.configDataTables = {
    "searching": true,
    "pageLength": 10,
    "responsive": true,
    "language": {
        "decimal": "",
        "emptyTable": "No hay información disponible",
        "info": "Viendo desde _START_ hasta  _END_ de _TOTAL_ elementos",
        "infoEmpty": "Viendo desde 0 hasta 0 de 0 elementos",
        "infoFiltered": "(filtrado desde _MAX_ elementos)",
        "infoPostFix": "",
        "thousands": ".",
        "lengthMenu": "Ver _MENU_ elementos",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "",
        "searchPlaceholder": "Buscar...",
        "zeroRecords": "No se encontraron coincidencias",
        "paginate": {
            "first": "Primero",
            "last": "Último",
            "next": "Próximo",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": activar ordenamiento de columnas ascendentemente",
            "sortDescending": ": activar ordenamiento de columnas descendentemente"
        }
    },
    "order": [],
    "initComplete": function (settings, json) {
        let searchContainer = $('.dataTables_filter').parent()
        searchContainer.addClass('ui form')
    },
}

/**
 * Configuración de Cropper
 * 
 * @property configCropper
 * @type {Object}
 */
globales.configCropper = {
    aspectRatio: 4 / 3,
    background: true,
    checkCrossOrigin: false,
    responsive: true,
    minCropBoxWidth: 1000,
    viewMode: 3
}

var SweetAlert = swal
var Alert = alertify
var Progress = NProgress

$(document).ready(function (e) {
    let calendarios = $('[calendar-js]')
    let tablas = $('[datatable-js]')

    calendarios.calendar(globales.configCalendar)
    tablas.DataTable(globales.configDataTables)

    var $btn = $('.btn-menu');

    $btn.click(function () {
		$('body').toggleClass('show');
		let altoMenu = $('.navbar').height()
		$('.overlay.menu-overlay').attr('style',`top:${altoMenu}px;`)
	});
	
	
	listenScrollForMenu()
	listenForResizeHeaderInternas()

})
function listenForResizeHeaderInternas(){
	let header = $('.encausa.header.sobreposicion')
	if(header.length>0){
		ajustarTamanno(header)
		$(window).resize(function(e){
			ajustarTamanno(header)
		})
	}
	function ajustarTamanno(header){
		let content = header.find('.content')
		if(content.length>0){
			let alturaContent = content.height()
			header.css({marginBottom:`${alturaContent}px`})
		}
	}
}
function listenScrollForMenu() {
	$(window).scroll(function (e) {
		let offset = $(this).scrollTop()
		let navbar = $('.navbar')
		let altoMenu = navbar.height()
		if (offset >= altoMenu) {
			navbar.addClass('scroll')
		} else {
			if (navbar.hasClass('scroll')) {
				setTimeout(() => {
					navbar.removeClass('scroll')
				}, 500)
			}
		}
		$('.overlay.menu-overlay').attr('style', `top:${altoMenu}px;`)
	})
}
var url_img = (src, url2 = 'news') => {
    return window.location.origin + `/encausa/admin/server/${url2}/${src}`;
}

$(".formularioContacto").click(function() {
	$(".formContacto").toggle('slow');
  });
