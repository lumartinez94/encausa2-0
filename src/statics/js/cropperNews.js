window.addEventListener('load', () => {

	$('.ui.dropdown').dropdown();

	let projectStatus = $(".ui.checkbox");
	projectValue = $(".status-value").val();
	if (projectValue == 1 || projectValue == "approved") {
		projectStatus.checkbox("set checked");
	}

	

	$('.ui.top.attached.tabular.menu .item').tab({
		context: 'parent'
	});

	let isEdit = false

	let options = {
		aspectRatio: 16 / 9,
		responsive: true,
		checkCrossOrigin: false,
		center: true
	};

	let optionsSm = {
		aspectRatio: 4 / 3,
		responsive: true,
		checkCrossOrigin: false,
		center: true
	};

	let cropper = new CropperAdapterComponent(
		{
			containerSelector: "[cropper-news]",
			minWidth: 1600,
			cropperOptions:{
				options,
			}
		}
			
	);

	
	let cropper2 = new CropperAdapterComponent(
		{
			containerSelector: "[cropper-newsm]",
			minWidth: 600,
			outputWidth: 600,
			cropperOptions: {
			optionsSm
			}
		}
			
	);


	let form = genericFormHandler("[action-form]", {
		onSetFormData: function (formData) {
			if (cropper.wasChanged()) {
				formData.set("portada", cropper.getFile("portada", 0.7));
				formData.set(
					"portadasm",
					cropper.getFile("portadasm", 0.7, 400)
				);

				formData.set("portadasm", cropper2.getFile("portadasm", 0.7, null, null, true));
			}
			return formData;
		}
	});

	
	let quillAdapter = new QuillAdapterComponent({
		containerSelector: '[quill-editor]',
		textareaTargetSelector: "textarea[name='description']",
		urlProcessImage: form.attr('quill'),
		nameOnRequest: 'image',
	})
	
	isEdit = form.find(`[name="id"]`).length > 0;

});
