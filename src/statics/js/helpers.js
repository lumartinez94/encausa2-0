/**
 * @method mensajeConfirmacion
 * @description Muestra un mensaje de confirmación con SwertAlert2
 * @param {String} title Título
 * @param {String} message Mensaje
 * @param {String} type Tipo de mensaje
 * @param {Function} onOk Callback en caso afirmativo
 * @param {Function} onCancel Callback en caso negativo
 */
function confirmMessage(
    title,
    message,
    onOk = null, 
    onCancel = null,
    type = 'warning',
    okText = 'Aceptar',
    cancelText='Cancelar'){
    SweetAlert({
        title: title,
        text: message,
        type: type,
        showCancelButton: true,
        showCloseButton: true,
        confirmButtonColor: 'green',
        cancelButtonColor: 'red',
        confirmButtonText: okText,
        cancelButtonText: cancelText
    }).then((result) => {
        if (result.value === true) {
            if(typeof onOk == 'function'){
                onOk()
            }
        } else {
            if(typeof onCancel == 'function'){
                onCancel()
            }
        }
    })
}

/**
 * @method loadModal
 * @description Modal de carga
 * @param {Promise} promise Promesa usada para cerrar el modal
 * cuando finalice
 */
function loadModal(promise){
    SweetAlert({
        allowOutsideClick: false,
        type: 'info',
        title: 'Cargando...',
        onOpen: (e) => {
            SweetAlert.showLoading()
        }
    })
    promise.then(function(){
        SweetAlert.close()
    }).catch(function(){
        SweetAlert.close()
    })
}

/**
 * @method successMessage
 * @description Mensaje modal con tipo success (SweetAlert2)
 * @param {String} title Título del mensaje
 * @param {String} message Mensaje
 */
function successMessage(title, message){
    title = title !== undefined ? title : ''
    message = message !== undefined ? message : ''
    SweetAlert({
        showCloseButton: true,
        type: 'success',
        title: title,
        text: message,
    })
}

/**
 * @method warningMessage
 * @description Mensaje modal con tipo warning (SweetAlert2)
 * @param {String} title Título del mensaje
 * @param {String} message Mensaje
 */
function warningMessage(title, message){
    title = title !== undefined ? title : ''
    message = message !== undefined ? message : ''
    SweetAlert({
        showCloseButton: true,
        type: 'warning',
        title: title,
        text: message,
    })
}

/**
 * @method questionMessage
 * @description Mensaje modal con tipo question (SweetAlert2)
 * @param {String} title Título del mensaje
 * @param {String} message Mensaje
 */
function questionMessage(title, message){
    title = title !== undefined ? title : ''
    message = message !== undefined ? message : ''
    SweetAlert({
        showCloseButton: true,
        type: 'question',
        title: title,
        text: message,
    })
}

/**
 * @method infoMessage
 * @description Mensaje modal con tipo info (SweetAlert2)
 * @param {String} title Título del mensaje
 * @param {String} message Mensaje
 */
function infoMessage(title, message){
    title = title !== undefined ? title : ''
    message = message !== undefined ? message : ''
    SweetAlert({
        showCloseButton: true,
        type: 'info',
        title: title,
        text: message,
    })
}

/**
 * @method errorMessage
 * @description Mensaje modal con tipo error (SweetAlert2)
 * @param {String} title Título del mensaje
 * @param {String} message Mensaje
 */
function errorMessage(title, message){
    title = title !== undefined ? title : ''
    message = message !== undefined ? message : ''
    SweetAlert({
        showCloseButton: true,
        type: 'error',
        title: title,
        text: message,
    })
}

/**
 * @method cropperToDataURL
 * @description Obtiene el string DataURL de la imagen de un objeto cropper
 * @param {Cropper} cropper Objeto Cropper
 * @param {Object} config Configuraciones
 * @param {Boolean} isJPG Convertir en JPEG
 * @return {String} DataURL del Cropper
 */
function cropperToDataURL(cropper, config = {}, isJPG = true){
    if(isJPG === true){
        return cropper.getCroppedCanvas(config).toDataURL("image/jpeg")
    }else{
        return cropper.getCroppedCanvas(config).toDataURL()
    }
}

/**
 * @method setCountdown
 * @description Crea una cuenta regresiva y lanza un evento
 * 'util-countdown' según el tiempo establecido
 * @param {String} dateLimit Fecha límite con los formatos:
 * AAAA-MM-DD HH:MM:SS == 2000-01-01 00:00:00
 * AAAA-MM-DD HH:MM == 2000-01-01 00:00
 * AAAA-MM-DD == 2000-01-01
 * @param {Number} time Tiempo de refresco en milisegundos
 * @return {void}
 */
function setCountdown(dateLimit, time = 1000) {

    var fechaLimite = new Date(dateLimit)//Fecha límite

    function lanzar(fechaLimite,interval) {

        var event = new Event('util-countdown')

        // Fecha actual
        var ahora = new Date()

        // Diferencia entre la fecha límite y la actual
        var tiempoFaltante = fechaLimite.getTime()-ahora.getTime()

        // Cálculo de dias, horas, minutos y segundos faltantes
        var dias = Math.floor(tiempoFaltante / (1000 * 60 * 60 * 24))
        var horas = Math.floor((tiempoFaltante % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
        var minutos = Math.floor((tiempoFaltante % (1000 * 60 * 60)) / (1000 * 60))
        var segundos = Math.floor((tiempoFaltante % (1000 * 60)) / 1000)

        var finalizado = tiempoFaltante <= 0 || (dias + horas + minutos + segundos) == 0

        // Datos del evento            
        event.tiempoFaltante = {
            dias: dias,
            horas: horas,
            minutos: minutos,
            segundos: segundos,
            string: dias + "d " + horas + "h " + minutos + "m " + segundos + "s",
            finalizado: finalizado
        }

        //Lanzar evento
        dispatchEvent(event)

        // Terminar
        if (finalizado) {
            clearInterval(interval)
        }
    }

    //Intervalo de un segundo
    var interval = setInterval(function () {

        lanzar(fechaLimite,interval)

    }, time)
    
    lanzar(fechaLimite,interval)

}

/**
 * Acortamiento de document.createElement
 * @param {string} tag 
 * @param {ElementCreationOptions} options
 * @returns {HTMLElememt}
 */
function _createElement(tag, options = null) {
	return document.createElement(tag, options)
}

/**
 * Crea un elemento y le asigna un innerHTML
 * @param {string} tag 
 * @param {string} innerHTML 
 * @param {ElementCreationOptions} options 
 * @returns {HTMLElememt}
 */
function createTagInnerHTML(tag, innerHTML = '', options = null) {
	let element = _createElement(tag, options)
	element.innerHTML = innerHTML
	return element
}

/**
 * 
 * @param {string} tag Etiquete HTML
 * @param {Array<Object>} attributes Array con objectos {attr:'',value:''} para agregar
 * atributos
 * @param {HTMLElement|Array<HTMLElement>} childs Elementos hijo
 * @param {string} innerHTML innerHTML introducido antes de agregar el hijo
 * @param {ElementCreationOptions} options 
 * @returns {HTMLElememt}
 */
function create(tag, attributes = null, childs = null, innerHTML = '', options = null) {

	let element = createTagInnerHTML(tag, innerHTML, options)

	if (Array.isArray(attributes)) {

		for (let i of attributes) {
			let validAttr = i.attr !== undefined && typeof i.attr == 'string'
			let validValue = i.value !== undefined
			if (validAttr && validValue) {
				if (typeof i.value == 'string') {
					element.setAttribute(i.attr, i.value.trim())
				} else if (Array.isArray(i.value)) {
					element.setAttribute(i.attr, i.value.join(' ').trim())
				} else {
					element.setAttribute(i.attr, i.value.toString().trim())
				}
			}
		}

	}

	if (childs !== null) {
		if (Array.isArray(childs)) {
			for (let i of childs) {
				element.appendChild(i)
			}
		} else {
			element.appendChild(childs)
		}
	}

	return element
}
