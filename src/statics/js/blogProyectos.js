(function($){
    
    $('.navbar .enlaces > li').removeClass('active');
    $('.navbar .enlaces > li')[3].classList.add('active');;
    $('.navbar ul.enlaces > li.active::before').css('left', '25px');

    var idem = $('input[name="idem"]');
    console.log(idem)
    $.when( requestData(idem) ).then( (response) => {
        //console.log(response);
        if (response.blog != null || response.blog != false) {
           
            var info = response.blog[0];
            
            $('#title').html(info.title_new);
            $('#sub_title').html(`"${info.sub_title_new}"`);
            $('#type').html(info.cat_name);
            $('#date').html(moment(info.created_at).locale('es').format('DD/MM/YY'));
            $('#body_text').html(info.body_new);
            $('#author_name').html(info.username);
            if (info.avatar != '') {
                $('#author_avatar').prop('src', info.avatar);
            }
            ///$('img[alt="cover"]').attr('src', url_img(info.source));

            $.each(response.newest, (i,v) => {
                var extra;
                var title = 'Anterior';
                if (i == 2) {
                    extra = 'left';
                    title = 'Siguiente';
                }
                $('#items').append(`
                    <div>
                        <div class="inverted ${extra}">
                            <h3> ${title} </h3>
                            <h3 class="title">
                                <a href="${v.seo}">
                                    ${v.title_new}
                                </a>
                            </h3>
                            <label class="date">
                                <span> ${v.cat_name} </span> | <span> ${moment(v.created_at).locale('es').format('DD/MM/YY')} </span>
                            </label>
                        </div>
                    </div>
                `);
            });

            $('.loader').animate({
                opacity: '0'
            }, 1000, () => {
                $('.loader').detach();
            });

        } else {
            window.location.href = "";
        }
    }).fail( () => {
        //swal('Lo sentimos','Ha ocurrido un error cargando la información','error');
    });

})(jQuery);


function requestData(idem) {
    return $.ajax({
        url: 'api/getProject',
        data: idem,
        method: 'POST',
        dataType: 'JSON'
    });
}
