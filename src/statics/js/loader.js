$(document).ready(function() {
	setTimeout(function() {
		$(".loader").fadeOut(1000);
	}, 2000);

	if ($("#cantidadNoticias").val() <= 9) {
		$(".cargarMas").hide();
	}

	contador = 1;
	let pages = 6;
	$("#cargarmas").on("click", function() {
		contador = contador + 1;

		var request = $.ajax({
			url: $("#articles").attr("url"),
			method: "POST",
			data: { contador: contador },
			dataType: "html",
			success: function(data) {
				result = data.split("|");

				$("#articles").append(result[0]);

				if (result[1] * pages >= result[2]) {
					$(".cargarMas").hide();
				}
			}
		});
	});
});
