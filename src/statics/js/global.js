$(document).ready(e => {
	var $btn = $(".btn-menu");

	$btn.click(function() {
		$("body").toggleClass("show");
		let altoMenu = $(".navbar").height();
		$(".overlay.menu-overlay").attr("style", `top:${altoMenu}px;`);
	});

	listenScrollForMenu();
	listenForResizeHeaderInternas();

	function listenForResizeHeaderInternas() {
		let header = $(".encausa.header.sobreposicion");
		if (header.length > 0) {
			ajustarTamanno(header);
			$(window).resize(function(e) {
				ajustarTamanno(header);
			});
		}
		function ajustarTamanno(header) {
			let content = header.find(".content");
			if (content.length > 0) {
				let alturaContent = content.height();
				header.css({ marginBottom: `${alturaContent}px` });
			}
		}
	}
	function listenScrollForMenu() {
		$(window).scroll(function(e) {
			let offset = $(this).scrollTop();
			let navbar = $(".navbar");
			let altoMenu = navbar.height();
			if (offset >= altoMenu) {
				navbar.addClass("scroll");
			} else {
				if (navbar.hasClass("scroll")) {
					setTimeout(() => {
						navbar.removeClass("scroll");
					}, 500);
				}
			}
			$(".overlay.menu-overlay").attr("style", `top:${altoMenu}px;`);
		});
	}
	var url_img = (src, url2 = "news") => {
		return window.location.origin + `/encausa/admin/server/${url2}/${src}`;
	};

	$(".formularioContacto").click(function() {
		$(".formContacto").toggle("slow");
	});

	$(window).click(e => {
		let target =
			$($(e.target).parents()[1]).attr("class") == "formularioContacto";

		if (!target) {
			$(".formContacto").hide("slow");
		}
	});
});
