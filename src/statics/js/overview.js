Progress.start()
$(document).ready(function (e) {
    $('[datatable-js]').DataTable().destroy()
    globales.configDataTables.rowReorder = true
    globales.configDataTables.colReorder = true
    let dataTable = $('[datatable-js]').DataTable(globales.configDataTables)

    $('.button.success').click(function (e) {
        successMessage('Titulo', 'Mensaje')
    })
    $('.button.warning').click(function (e) {
        warningMessage('Titulo', 'Mensaje')
    })
    $('.button.question').click(function (e) {
        questionMessage('Titulo', 'Mensaje')
    })
    $('.button.info').click(function (e) {
        infoMessage('Titulo', 'Mensaje')
    })
    $('.button.error').click(function (e) {
        errorMessage('Titulo', 'Mensaje')
    })
    $('.button.link').click(function (e) {
        SweetAlert({
            showCloseButton: true,
            type: 'error',
            title: 'Título...',
            text: 'Mensaje error',
            footer: '<a href>Con un enlace</a>',
        })
    })
    $('.button.action').click(function (e) {
        confirmMessage('Title', 'Mensaje', () => {
            console.log('OK')
        }, () => {
            console.log('Cancel')
        })
    })
    $('.button.charge').click(function (e) {
        loadModal(new Promise((resolve, reject) => {
            setTimeout(function (e) {
                resolve()
            }, 3000)
        }))


    })

    $('.alert1').click(function (e) {
        Alert.error('Mensaje del error.', 2, function (e) {
            console.log('Mensaje cerrado')
        })
        Alert.message('Mensaje del message.', 2, function (e) {
            console.log('Mensaje cerrado')
        })
        Alert.notify('Mensaje del notify.', 'clase-custom', 2, function (e) {
            console.log('Mensaje cerrado')
        })
        Alert.success('Mensaje del success.', 2, function (e) {
            console.log('Mensaje cerrado')
        })
        Alert.warning('Mensaje del warning.', 2, function (e) {
            console.log('Mensaje cerrado')
        })
    })

    let form = $('.ui.form.example')
    let fechaInput = form.find("[name='fecha']")
    let horaInput = form.find("[name='hora']")

    horaInput.mask('00:00:00', {
        placeholder: 'HORAS:MINUTOS:SEGUNDOS'
    })

    form.form({
        fields: {
            fecha: {
                identifier: 'fecha',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Ingresa una fecha.'
                    }
                ]
            },
            hora: {
                identifier: 'fecha',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Ingresa un valor'
                    }
                ]
            }
        },
        onFailure: function(errors,fields){
            console.log(errors)
            console.log(fields)
        }
    })
    form.submit(function (e) {
        e.preventDefault()
        let isValid = $(this).form('is valid')
        if(!isValid){
        }
        return false
    })

    Progress.done()
})